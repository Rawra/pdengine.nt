# PDEngine.NT.LDR


## What it is

This is a extension to pdengine.nt featuring some low-level functionality for interacting with the windows libloader api (LDR) specifically.

## Features

- Utility function for adding custom library search paths via path and by hooking low level system functions to search other places aswell (LdrpLoadDll)

## Examples

The following example will result in the process-wide path being altered to include the current executing assembly
and additionally will hook LdrpLoadDll to forcefully search the current directory aswell.
```cs
	PDProcessExplorer32.Instance.Initialize("PDEngine.NTExApp.exe");
	LibraryLoaderManager.Instance.TryHookLowLevelLoader();
```

For external directories (LdrpLoadDll only) there is a user API provided:
```cs
	// Add custom paths like so
	LibraryLoaderManager.Instance.AddLibrarySearchPaths("C:\", "D:\");
	// or
	LibraryLoaderManager.Instance.AddLibrarySearchPath("C:\");
	
	// Remove them like so (HashSet check)
	LibraryLoaderManager.Instance.RemoveLibrarySearchPath("C:\");
```