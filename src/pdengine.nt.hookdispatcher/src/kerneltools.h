#pragma once

NTSTATUS MyRtlInitUnicodeString(PUNICODE_STRING DestinationString, PCWSTR SourceString);
VOID MyFreeUnicodeString(PUNICODE_STRING UnicodeString);