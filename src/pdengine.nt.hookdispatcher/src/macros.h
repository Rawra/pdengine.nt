#pragma once

#define DLLEXPORT extern "C" __declspec(dllexport) 
#define STDCALL __stdcall
#define CDECL __cdecl
#define FASTCALLL __fastcall
#define NAKED __declspec(naked)

#define VAR_NAME(x) #x
#define STR(s) L ## s
#define CH(c) L ## c
#define DIR_SEPARATOR L'\\'

// Macro that creates a Detour using PolyHook2 using Function Name, Address (A Typedef has to exist!)
#define CREATE_DETOUR_V2(func_name, address)		\
PLH::x86Detour* func_name##_detour;						\
uint64_t func_name##_trampoline = NULL;					\
uint64_t func_name##_address = (uint64_t)(address);	\
func_name##_t game_##func_name = (func_name##_t)(func_name##_address);

#define SETUP_AOB_DETOUR_V2_FORWARD(func_name) \
extern PLH::x86Detour* func_name##_detour; \
extern uint64_t func_name##_trampoline;	\
extern uint64_t func_name##_address; \
extern func_name##_t func_name##_game;

#define SETUP_AOB_DETOUR_V2(func_name) \
PLH::x86Detour* func_name##_detour; \
uint64_t func_name##_trampoline = NULL;	\
uint64_t func_name##_address = 0; \
func_name##_t func_name##_game = (func_name##_t)(func_name##_address);

#define UPDATE_AOB_DETOUR_V2(func_name, signature) \
func_name##_address = sw3se::common::baseAddress + (uint64_t)sw3se::common::patternfind((unsigned char*)sw3se::common::baseAddress, sw3se::common::baseModuleInfo.SizeOfImage, signature); \
func_name##_game = (func_name##_t)(func_name##_address); 

#define APPLY_AOB_DETOUR_V2(func_name, signature) \
UPDATE_AOB_DETOUR_V2(func_name, signature); \
APPLY_DETOUR_V2(func_name, sw3se::common::dis);

// Macro that applies a Detour created via CREATE_DETOUR Macros.
#define APPLY_DETOUR_V2(func_name, disassembler) \
spdlog::debug("Creating Hook: 0x{1:x}@{0}", #func_name, (uintptr_t)func_name##_address); \
func_name##_detour = new PLH::x86Detour((uint64_t)func_name##_game, (uint64_t)&func_name##_hook, &func_name##_trampoline); \
if (!func_name##_detour->hook()) { \
	spdlog::error("Error while Hooking {0}", #func_name); \
} \
spdlog::debug("Created Tramp: 0x{0:x}", (uintptr_t)func_name##_trampoline); \

// Macro that applies a Detour created via CREATE_DETOUR Macros.
#define APPLY_DETOUR_NOLOG_V2(func_name, disassembler) \
func_name##_detour = new PLH::x86Detour((uint64_t)func_name##_game, (uint64_t)&func_name##_game, &func_name##_trampoline, ##disassembler); \
if (!func_name##_detour->hook()) { \
	MessageBoxA(0, "FATAL ERROR WHILE HOOKING", #func_name, 0); \
}
/*
// Macro that applies a Detour created via CREATE_DETOUR Macros, with the exception that this macro calls a different named hook.
#define APPLY_DETOUR_EX(func_name, disassembler) \
detour_##func_name = new PLH::x86Detour((uint64_t)game_##func_name, (uint64_t)&asmcall_hook_##func_name, &trampoline_##func_name, ##disassembler); \
if (!detour_##func_name->hook()) {						\
	spdlog::error("Error while hooking {0}", #func_name);\
}	*/

#define WPM_Value_DWORD(__Name__, __HProc__, __Address__, __Value__, __OldValue__, __NumBytesWritten__) \
if (!WriteProcessMemory(__HProc__, \
(LPVOID)__Address__, \
&__Value__, sizeof(DWORD), \
&__NumBytesWritten__)) { \
spdlog::critical("Error: Couldn't WPM_VALUE_UL {0} from {1} to {2}", #__Name__, __OldValue__, __Value__); \
}

#define WPM_Value_BYTE(__Name__, __HProc__, __Address__, __Value__, __OldValue__, __NumBytesWritten__) \
if (!WriteProcessMemory(__HProc__, \
(LPVOID)__Address__, \
&__Value__, sizeof(char), \
&__NumBytesWritten__)) { \
spdlog::critical("Error: Couldn't WPM_VALUE_BYTE {0} from {1} to {2}", #__Name__, __OldValue__, __Value__); \
}

