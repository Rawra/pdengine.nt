#include "pch.h"

/// <summary>
/// RtlInitUnicodeString custom impl
/// </summary>
/// <param name="DestinationString"></param>
/// <param name="SourceString"></param>
/// <returns></returns>
NTSTATUS MyRtlInitUnicodeString(PUNICODE_STRING DestinationString, PCWSTR SourceString) {
    if (DestinationString == nullptr || SourceString == nullptr)
        return STATUS_INVALID_PARAMETER;

    size_t sourceLength = wcslen(SourceString);
    size_t bufferSize = (sourceLength + 1) * sizeof(wchar_t); // +1 for null-terminator

    // Allocate memory for the buffer
    DestinationString->Buffer = (wchar_t*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bufferSize);
    if (DestinationString->Buffer == nullptr)
        return STATUS_NO_MEMORY;

    // Copy the source string to the buffer
    memcpy(DestinationString->Buffer, SourceString, bufferSize);

    // Set the length and maximum length
    DestinationString->Length = static_cast<USHORT>(sourceLength * sizeof(wchar_t));
    DestinationString->MaximumLength = static_cast<USHORT>(bufferSize);

    return (NTSTATUS)0;
}

/// <summary>
/// FreeUnicodeString custom impl
/// </summary>
/// <param name="UnicodeString"></param>
/// <returns></returns>
VOID MyFreeUnicodeString(PUNICODE_STRING UnicodeString) {
    if (UnicodeString == nullptr)
        return;

    if (UnicodeString->Buffer != nullptr) {
        HeapFree(GetProcessHeap(), 0, UnicodeString->Buffer);
        UnicodeString->Buffer = nullptr;
        UnicodeString->Length = 0;
        UnicodeString->MaximumLength = 0;
    }
}