#include "pch.h"
#include "LdrController.h"
/*

    CLASS FUNCTIONS

*/

LdrControllerX86::LdrControllerX86(const uint64_t ldrpLoadDllFnAddress) {
    this->detourHandle = new PLH::x86Detour(ldrpLoadDllFnAddress, (uint64_t)&LdrpLoadDll_hook_impl, &this->ldrploaddll_trampoline);
}

void LdrControllerX86::SetGlobalLdrpLoadDllFunction() {
    this->ldrploaddll_function = (ldrploadll_t)(ldrploaddll_trampoline);
    ldrploaddll_function_global = this->ldrploaddll_function;
}

bool LdrControllerX86::InstallLdrpLoadDllHook() {
    if (!this->detourHandle->hook())
        return false;

    SetGlobalLdrpLoadDllFunction();

    return true;
}

bool LdrControllerX86::UnInstallLdrpLoadDllHook() {
    if (ldrploaddll_function_global == nullptr)
        return false;

    if (!this->detourHandle->unHook())
        return false;

    return true;
}

bool LdrControllerX86::AddLowLevelSearchDirectoryEx(const wchar_t* paths[], int count) {
    for (int i = 0; i < count; i++) {
        ldrploaddll_search_paths_global.insert(paths[i]);
    }
    return true;
}

bool LdrControllerX86::RemoveLowLevelSearchDirectory(const wchar_t* path) {
    return ldrploaddll_search_paths_global.erase(path);
}

bool LdrControllerX86::AddLowLevelSearchDirectory(const wchar_t* path) {
    //std::wcout << L"AddLowLevelSearchDirectory: Adding [" << path << L"]" << std::endl;
    ldrploaddll_search_paths_global.insert(std::wstring(path));
    return true;
}

/*
    GLOBAL HOOKS

*/
//#define LOG_DLLS 1
#if LOG_DLLS

static FILE* frOut;
static bool frInitialized = false;
#endif

NTSTATUS static __fastcall LdrpLoadDll_hook_impl(PUNICODE_STRING DllName, LDR_UNKSTRUCT* DllPathInited, ULONG Flags, LDR_DATA_TABLE_ENTRY** DllEntry) {
    UNICODE_STRING dllNameModified = *DllName;
    std::wstring dllNameWStr(DllName->Buffer, DllName->Length / sizeof(wchar_t));
#if LOG_DLLS
    if (!frInitialized) {
        AllocConsole();
        freopen_s(&frOut, "CONOUT$", "w", stdout);
        frInitialized = true;
    }
    std::wcout << L"LdrpLoadDll_hook_impl: [" << dllNameWStr << L"]" << std::endl;
#endif

    for (auto path : ldrploaddll_search_paths_global) {
        //std::wcout << L"Search Path: [" << path << L"]" << std::endl;
        std::filesystem::path dllNameTestPath(path.c_str());
        dllNameTestPath.append(dllNameWStr.c_str());
#if LOG_DLLS
        std::wcout << L"LdrpLoadDll_hook_impl: dllNameTestPath=[" << dllNameTestPath << L"]" << std::endl;
#endif

        if (std::filesystem::exists(dllNameTestPath)) {
            MyRtlInitUnicodeString(&dllNameModified, dllNameTestPath.c_str());
        }
    }

    return ldrploaddll_function_global(&dllNameModified, DllPathInited, Flags, DllEntry);
}

/*
    C API
    EXPORTED FUNCTIONS
*/

LdrControllerX86* CDECL ldrpcontroller32_create(uint64_t ldrpLoadDllFnAddress) {
    return new LdrControllerX86(ldrpLoadDllFnAddress);
}

bool CDECL ldrpcontroller32_addlowlevelsearchdirectoryex(LdrControllerX86* handle, const wchar_t* paths[], int count) {
    return handle->AddLowLevelSearchDirectoryEx(paths, count);
}

bool CDECL ldrpcontroller32_addlowlevelsearchdirectory(LdrControllerX86* handle, const wchar_t* path) {
    return handle->AddLowLevelSearchDirectory(path);
}

bool CDECL ldrpcontroller32_removelowlevelsearchdirectory(LdrControllerX86* handle, const wchar_t* path) {
    return handle->RemoveLowLevelSearchDirectory(path);
}

bool CDECL ldrpcontroller32_installhooks(LdrControllerX86* handle) {
    return handle->InstallLdrpLoadDllHook();
}

bool CDECL ldrpcontroller32_uninstallhooks(LdrControllerX86* handle) {
    return handle->UnInstallLdrpLoadDllHook();
}