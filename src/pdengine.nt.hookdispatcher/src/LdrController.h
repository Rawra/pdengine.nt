#pragma once

struct LDR_UNKSTRUCT
{
    PWSTR pInitNameMaybe;
    __declspec(align(16)) PWSTR Buffer;
    int Flags;
    PWSTR pDllName;
    char Pad1[84];
    BOOLEAN IsInitedMaybe;
    char Pad2[3];
};
typedef NTSTATUS(__fastcall* ldrploadll_t)(PUNICODE_STRING DllName, LDR_UNKSTRUCT* DllPathInited, ULONG Flags, LDR_DATA_TABLE_ENTRY** DllEntry);
NTSTATUS __fastcall LdrpLoadDll_hook_impl(PUNICODE_STRING DllName, LDR_UNKSTRUCT* DllPathInited, ULONG Flags, LDR_DATA_TABLE_ENTRY** DllEntry);

NTSTATUS static __fastcall LdrpLoadDll_hook_impl(PUNICODE_STRING DllName, LDR_UNKSTRUCT* DllPathInited, ULONG Flags, LDR_DATA_TABLE_ENTRY** DllEntry);

ldrploadll_t ldrploaddll_function_global;
std::unordered_set<std::wstring> ldrploaddll_search_paths_global;

class LdrControllerX86 {
public:
    LdrControllerX86(const uint64_t ldrpLoadDllFnAddress);
    bool AddLowLevelSearchDirectoryEx(const wchar_t* paths[], int count);
    bool AddLowLevelSearchDirectory(const wchar_t* path);
    bool RemoveLowLevelSearchDirectory(const wchar_t* path);

    bool InstallLdrpLoadDllHook();
    bool UnInstallLdrpLoadDllHook();

private:
    void SetGlobalLdrpLoadDllFunction();

    ldrploadll_t ldrploaddll_function = nullptr;
    uint64_t ldrploaddll_trampoline = 0;
    PLH::x86Detour* detourHandle = nullptr;
};

DLLEXPORT LdrControllerX86* CDECL ldrpcontroller32_create(uint64_t ldrpLoadDllFnAddress);
DLLEXPORT bool CDECL ldrpcontroller32_addlowlevelsearchdirectoryex(LdrControllerX86* handle, const wchar_t* paths[], int count);
DLLEXPORT bool CDECL ldrpcontroller32_addlowlevelsearchdirectory(LdrControllerX86* handle, const wchar_t* path);
DLLEXPORT bool CDECL ldrpcontroller32_removelowlevelsearchdirectory(LdrControllerX86* handle, const wchar_t* path);

DLLEXPORT bool CDECL ldrpcontroller32_installhooks(LdrControllerX86* handle);
DLLEXPORT bool CDECL ldrpcontroller32_uninstallhooks(LdrControllerX86* handle);