#ifndef PCH_H
#define PCH_H

#include <windows.h>
//#include <ntdef.h>
#include <winternl.h>

#include <string>
#include <vector>
#include <cstdint>
#include <exception>
#include <filesystem>
#include <unordered_set>

#include "macros.h"
#include <polyhook2/Detour/x86Detour.hpp>
#include <polyhook2/ZydisDisassembler.hpp>

#include "kerneltools.h"

#endif //PCH_H
