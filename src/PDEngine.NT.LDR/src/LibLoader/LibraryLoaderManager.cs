﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PDEngine.NT.LDR.LibLoader
{
    public class LibraryLoaderManager
    {
        private static readonly Lazy<LibraryLoaderManager> lazy = new Lazy<LibraryLoaderManager>(() => new LibraryLoaderManager());

        public static LibraryLoaderManager Instance { get { return lazy.Value; } }

        private LibraryLoaderManager()
        {

        }

        /// <summary>
        /// Public Library Manager API
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool AddLibrarySearchPath(string path)
        {
            return AddLibrarySearchPathHighLevel(path);
        }

        public bool AddLibrarySearchPaths(string[] paths)
        {
            return AddLibrarySearchPathsHighLevel(paths);
        }

        public bool RemoveLibrarySearchPath(string path)
        {
            return RemoveLibrarySearchPathHighLevel(path);
        }

        /// <summary>
        /// Library Manager (High-Level) API
        /// Will change process-wide path only
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool AddLibrarySearchPathHighLevel(string path)
        {
            // Make sure path doesnt exist already
            string? currentPath = System.Environment.GetEnvironmentVariable("PATH");
            if (currentPath == null)
                throw new Exception("Could not retrieve system PATH");

            if (currentPath.Contains(path))
                return false;

            //Console.WriteLine($"AddLibrarySearchPathHighLevel: currentPath pre add: {currentPath}");

            // Add to path to PATH
            System.Environment.SetEnvironmentVariable("PATH", currentPath + $";{path}");

            //Console.WriteLine($"AddLibrarySearchPathHighLevel: currentPath post add: {currentPath}");
            return true;
        }
        
        private bool AddLibrarySearchPathsHighLevel(string[] paths)
        {
            foreach (string path in paths)
            {
                if (!AddLibrarySearchPathHighLevel(path))
                    return false;
            }
            return true;
        }

        private bool RemoveLibrarySearchPathHighLevel(string path)
        {
            // Make sure the path exists
            string? currentPath = System.Environment.GetEnvironmentVariable("PATH");
            if (currentPath == null)
                throw new Exception("Could not retrieve system PATH");

            if (!currentPath.Contains(path))
                return false;

            //Console.WriteLine($"RemoveLibrarySearchPathHighLevel: currentPath pre remove: {currentPath}");
            // Remove path from PATH
            //System.Environment.SetEnvironmentVariable("PATH", currentPath + $";{path}");
            int startIndex = currentPath.IndexOf(path, StringComparison.InvariantCultureIgnoreCase);
            currentPath.Remove(startIndex, path.Length);

            // Make sure to delete trailing semicolon
            if (currentPath.Length > startIndex + 1)
                if (currentPath[startIndex + 1] == ';')
                    currentPath.Remove(startIndex + 1, 1);

            //Console.WriteLine($"RemoveLibrarySearchPathHighLevel: currentPath post remove: {currentPath}");
            System.Environment.SetEnvironmentVariable("PATH", currentPath);

            return true;
        }

    }
}
