﻿using PDEngine.NT.XAnyStructs;
using PDEngine.NT.X86Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using PDEngine.NT.PInvokeUnmapped;
using System.Security;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.IO;
using PDEngine.NT.Utility;
using PInvoke.PolyHook2;

namespace PDEngine.NT.LDR.LibLoader
{
    public unsafe sealed class LibraryLoaderLowLevelManagerI386 : IDisposable
    {
        private static readonly Lazy<LibraryLoaderLowLevelManagerI386> lazy = new Lazy<LibraryLoaderLowLevelManagerI386>(() => new LibraryLoaderLowLevelManagerI386());

        public static LibraryLoaderLowLevelManagerI386 Instance { get { return lazy.Value; } }

        /// <summary>
        /// Set of paths to be searched for when windows searches for a dll
        /// </summary>
        public HashSet<string> SearchPaths;

        private delegate NTSTATUS ldrploaddll_delegate(UNICODE_STRING* dllName, LDR_UNKSTRUCT* DllPathInited, ulong Flags, _LDR_DATA_TABLE_ENTRY** DllEntry);
        private X86ManagedFunctionHook<ldrploaddll_delegate> _LdrpLoadDllHook;

        private LibraryLoaderLowLevelManagerI386()
        {
            _LdrpLoadDllHook = new X86ManagedFunctionHook<ldrploaddll_delegate>((IntPtr)NtDllEx.Instance.LdrpLoadDllAddress, LdrpLoadDll_hook_impl, doAutoHook: false);
            SearchPaths = new HashSet<string>();
        }

        public bool Hook()
        {
            return _LdrpLoadDllHook.Hook();
        }

        public bool UnHook() 
        {
            return _LdrpLoadDllHook.UnHook();
        }

        public bool TryRemoveSearchPath(string path)
        {
            if (SearchPaths.Count == 0)
                return false;
            return SearchPaths.Remove(path);
        }

        public bool TryAddSearchPath(string path)
        {
            return SearchPaths.Add(path);
        }
        public bool TryAddSearchPaths(string[] path)
        {
            foreach (var s in path)
            {
                if (!SearchPaths.Add(s))
                    return false;
            }
            return true;
        }

        public void Dispose()
        {
            _LdrpLoadDllHook.UnHook();
        }

        /// <summary>
        /// Lowest Level NT!LdrpLoadDll hook to manipulate the default directories
        /// windows search for the specified dllName.
        /// 
        /// Use with caution, if this code triggers (as in: this very function) a LoadLibrary event
        /// it will cause a softlock and will cause a stackoverflow exception shortly due to recursive calls.
        /// </summary>
        /// <param name="dllName">Dll file name</param>
        /// <param name="DllPathInited">Undocumented struct</param>
        /// <param name="Flags">LoadLibrary flags</param>
        /// <param name="DllEntry">LDR Entry</param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [SuppressUnmanagedCodeSecurity]
        internal static NTSTATUS LdrpLoadDll_hook_impl(UNICODE_STRING* dllName, LDR_UNKSTRUCT* DllPathInited, ulong Flags, _LDR_DATA_TABLE_ENTRY** DllEntry)
        {
            UNICODE_STRING dllNameModified = *dllName;
            string dllNameTemp = dllName->ToString();
            //Console.WriteLine($"LdrpLoadDll_hook_impl: dllName={dllNameTemp}");

            foreach (string path in Instance.SearchPaths)
            {
                string testFilePath = Path.Combine(path, dllNameTemp);
                if (File.Exists(testFilePath))
                {
                    //Console.WriteLine($"exists: {testFilePath}");
                    NtosKrnl.MyRtlInitUnicodeString(&dllNameModified, testFilePath);
                    return Instance._LdrpLoadDllHook.Trampoline(&dllNameModified, DllPathInited, Flags, DllEntry);
                }
            }
            //Console.WriteLine($"Returning");
            return Instance._LdrpLoadDllHook.Trampoline(dllName, DllPathInited, Flags, DllEntry);
        }
    }
}
