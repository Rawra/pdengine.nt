﻿using MutatedHooks;
using PDEngine.NT.LDR.LibLoader;
using PDEngine.NT.Managers;
using PDEngine.NT.PInvoke;
using PDEngine.NT.Utility;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace PDEngine.NTExApp
{
    internal class Program
    {
        //[DllImport("discord_game_sdk", ExactSpelling = true)]
        //private static extern int DiscordCreate(uint version, IntPtr createParams, out IntPtr manager);
        static unsafe void Main(string[] args)
        {
            //if (Kernel32.LoadLibraryA("discord_game_sdk.dll") == IntPtr.Zero)
            //    Console.WriteLine($"BEFORE Library not found");
            //else
            //    Console.WriteLine($"BEFORE Library found");

            //LibraryLoaderLowLevelManager.Instance.TryAddSearchPath(Path.Combine(Environment.CurrentDirectory, "x86_64"));
            //LibraryLoaderManager.Instance.AddLibrarySearchPath(Path.Combine(Environment.CurrentDirectory, "x86_64"));
            //DiscordCreate(0, IntPtr.Zero, out IntPtr manager);
            //if (Kernel32.LoadLibraryA("discord_game_sdk.dll") == IntPtr.Zero)
            //    Console.WriteLine($"AFTER Library not found");
            //else
            //    Console.WriteLine($"AFTER Library found");


            if (Environment.Is64BitProcess)
            {
                Console.WriteLine($"TEB Address:\t\t\t0x{PDEngine.NT.Utility.TEB.GetTEB().ToString("X16")}");
                Console.WriteLine($"PEB Address:\t\t\t0x{PDEngine.NT.Utility.PEB.GetPEB().ToString("X16")}");
                Console.WriteLine($"LDR Address:\t\t\t0x{new IntPtr(PDProcessExplorer64.Instance.CurrentPEB->Ldr).ToString("X16")}");
                Console.WriteLine($"Image Base Address:\t\t0x{PDProcessExplorer64.Instance.GetImageBaseAddress().ToString("X16")}");
                Console.WriteLine($"Image Size:\t\t\t0x{PDProcessExplorer64.Instance.GetSizeOfImage().ToString("X16")}");
            } 
            else
            {
                Console.WriteLine($"TEB Address:\t\t\t0x{PDEngine.NT.Utility.TEB.GetTEB().ToString("X8")}");
                Console.WriteLine($"PEB Address:\t\t\t0x{PDEngine.NT.Utility.PEB.GetPEB().ToString("X8")}");
                Console.WriteLine($"LDR Address:\t\t\t0x{new IntPtr(PDProcessExplorer32.Instance.CurrentPEB->Ldr).ToString("X6")}");
                Console.WriteLine($"Image Base Address:\t\t0x{PDProcessExplorer32.Instance.GetImageBaseAddress().ToString("X6")}");
                Console.WriteLine($"Image Size:\t\t\t0x{PDProcessExplorer32.Instance.GetSizeOfImage().ToString("X6")}");
            }

            RDTSC.IsRDTSCPSupported();
            UInt64 val = 0xDEADBEEFCAFEBABE;
            UInt32 cpu = 0;
            var pid = Process.GetCurrentProcess().Id;   
            for (int i = 0; i <= 10; i++)
            {
                Console.WriteLine($"FP={new IntPtr(RDTSC.GetRDTSCPEx).ToString("X8")}");
                Console.ReadLine();
                //RDTSC.GetRDTSCPEx((UInt32*)(&val), (UInt32*)((UInt32)(&val) + 4), &cpu);
                RDTSC.GetRDTSCPEx((UInt32*)(&val), (UInt32*)((IntPtr.Add((IntPtr)(&val), 4))), &cpu);
                Console.WriteLine($"GetRDTSCPEx VALUE..: {val}\tCPU={cpu.ToString("X8")}\tProcessID={pid}");
                Console.WriteLine($"RDTSCP=..............: {RDTSC.GetRDTSC()}");
            }

            Console.WriteLine($"GetRDTSC_X86 VALUEEX: {val.ToString("X")}");
            Console.WriteLine($"FP={new IntPtr(RDTSC.GetChipVendorName).ToString("X16")}");
            Console.WriteLine("Any key to retrieve CPU Vendor Name");
            Console.ReadKey();

            byte* ptr = stackalloc byte[12] { 0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE, 0xBA, 0xBE, 0xDE, 0xAD, 0xBE, 0xEF };
            IntPtr ptr2 = RDTSC.GetChipVendorName((IntPtr)ptr);
            //ptr[12] = 0x00;
            Console.WriteLine($"CPU Vendor Name: {UTF8Encoding.UTF8.GetString(ptr, 12)}");
            Console.WriteLine($"CPU Vendor NameEX: {new IntPtr(ptr).ToString("X16")}");
            Console.WriteLine($"CPU Vendor NameEX2: {ptr2.ToString("X16")}");

            Console.WriteLine($"RDTSC={RDTSC.GetRDTSC()}");
            Console.WriteLine($"RDTSCP={RDTSC.GetRDTSCP()}");
            Console.WriteLine($"RDTSCANDCPUID={RDTSC.GetRDTSCANDCPUID()}");
            Console.WriteLine($"Timestamp={RDTSC.Timestamp()}");
            Console.WriteLine($"TimestampP={RDTSC.TimestampP()}");

            Console.WriteLine("Any key to exit");
            Console.ReadLine();
        }
    }
}
