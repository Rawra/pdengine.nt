﻿using Reloaded.Memory.Sigscan.Definitions.Structs;
using Reloaded.Memory.Sigscan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using PDEngine.NT.Managers;
using PDEngine.NT.X86Structs;
using System.Net;
using System.ComponentModel;

namespace PDEngine.NT.Memory
{
    public class FunctionTools
    {
        /// <summary>
        /// Try to search for a function starting at a memory address via AOB pattern
        /// with additional fallback AOB pattern
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="scanner">The scanner to use</param>
        /// <param name="signature">The signature to search for</param>
        /// <param name="fallbackSignature">The fallback signature to search for (ex: x86_64 pattern)</param>
        /// <param name="baseAddress">The address to search at</param>
        /// <param name="function">The function found</param>
        /// <param name="address">The address of the function</param>
        /// <returns>Success</returns>
        public unsafe static bool TryScanForFunction<T>(ref Scanner scanner, string signature, string fallbackSignature, IntPtr baseAddress, out T? function, out IntPtr address, bool isFallback = false)
        {
            function = default;
            address = IntPtr.Zero;
            PatternScanResult scanResult = scanner.FindPattern(signature);
            if (!scanResult.Found)
                if (isFallback)
                    return false;
                else 
                    return TryScanForFunction<T>(ref scanner, fallbackSignature, string.Empty, baseAddress, out function, out address, isFallback: true);

            address = baseAddress + scanResult.Offset;
            function = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)address);
            return true;
        }

        /// <summary>
        /// Try to search for a function starting at a memory address via AOB pattern
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="scanner">The scanner to use</param>
        /// <param name="signature">The signature to search for</param>
        /// <param name="baseAddress">The address to search at</param>
        /// <param name="function">The function found</param>
        /// <param name="address">The address of the function</param>
        /// <returns>Success</returns>
        public unsafe static bool TryScanForFunction<T>(ref Scanner scanner, string signature, IntPtr baseAddress, out T? function, out IntPtr address)
        {
            function = default;
            address = IntPtr.Zero;
            PatternScanResult scanResult = scanner.FindPattern(signature);
            if (!scanResult.Found)
                return false;

            address = baseAddress + scanResult.Offset;
            function = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)address);
            return true;
        }

        public unsafe static bool TryScanForSignature(ref Scanner scanner, string signature, IntPtr baseAddress, out IntPtr address)
        {
            address = IntPtr.Zero;
            PatternScanResult scanResult = scanner.FindPattern(signature);
            if (!scanResult.Found)
                return false;

            address = baseAddress + scanResult.Offset;
            return true;
        }

    }
}
