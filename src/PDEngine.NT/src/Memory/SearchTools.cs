﻿using Reloaded.Memory.Sigscan.Definitions.Structs;
using Reloaded.Memory.Sigscan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.Memory
{
    public class SearchTools
    {
        /// <summary>
        /// Scan for a pattern with a given scanner, AOB Pattern and a fallback pattern if the former is not found.
        /// The to be searched memory region is defined by referenced scanner, if a result is found this function will return
        /// true and the "address" parameter will contain the found pattern address.
        /// </summary>
        /// <param name="scanner">The scanner to be used (will define memory region, memory size, etc)</param>
        /// <param name="signature">The pattern to scan for</param>
        /// <param name="fallbackSignature">If the signature is not found, try this one instead</param>
        /// <param name="baseAddress">The base address to respect (gets added to out address)</param>
        /// <param name="address">The address of the found pattern</param>
        /// <param name="isFallback">Used for the fallback invoke (do not use)</param>
        /// <returns></returns>
        public unsafe static bool TryScanForPattern(ref Scanner scanner, string signature, string fallbackSignature, IntPtr baseAddress, out IntPtr address, bool isFallback = false)
        {
            address = IntPtr.Zero;
            PatternScanResult scanResult = scanner.FindPattern(signature);
            if (!scanResult.Found)
                if (isFallback)
                    return false;
                else
                    return TryScanForPattern(ref scanner, fallbackSignature, string.Empty, baseAddress, out address, isFallback: true);

            address = (IntPtr)((ulong)scanResult.Offset + (ulong)baseAddress);
            return true;
        }
    }
}
