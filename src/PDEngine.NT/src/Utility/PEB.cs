﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Iced.Intel;
using PDEngine.NT.PInvoke;
using PDEngine.NT.XAnyStructs;

namespace PDEngine.NT.Utility
{
    public unsafe class PEB : IDisposable
    {
        private static readonly Lazy<PEB> lazy = new Lazy<PEB>(() => new PEB());

        public static PEB Instance { get { return lazy.Value; } }

        private PEB()
        {
            _stubAddress = ConstructStub();
            GetAddress = (delegate* unmanaged[Stdcall]<IntPtr>)_stubAddress;
        }

        /// <summary>
        /// Address of the stub with the generated assembly code
        /// </summary>
        private IntPtr _stubAddress = IntPtr.Zero;

        /// <summary>
        /// Retrieve the address of the PEB
        /// </summary>
        public delegate* unmanaged[Stdcall]<IntPtr> GetAddress;
        public static IntPtr GetPEB() => Instance.GetAddress();

        /// <summary>
        /// Never call this and try to access GetAddress()
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void Dispose()
        {
            if (_stubAddress == IntPtr.Zero)
                return;

            // Free the allocated memory to restore the previous state (dwSize 0 due to MEM_RELEASE)
            if (!Kernel32.VirtualFree(_stubAddress, 0, Kernel32.MEM_RELEASE))
                throw new Exception("VirtualFree Failed to release memory");
        }

        /// <summary>
        /// Retrieve the Process Environment Block address (PEB)
        /// in a safe manner
        /// </summary>
        /// <returns></returns>
        public static IntPtr GetPEBSafe()
        {
            PROCESS_BASIC_INFORMATION pbi = default;

            // Call NtQueryInformationProcess to retrieve basic information about the process
            NTSTATUS status = NtDll.NtQueryInformationProcess(
                Process.GetCurrentProcess().Handle,
                NtDll.ProcessBasicInformation,
                &pbi,
                sizeof(PROCESS_BASIC_INFORMATION),
                out int returnedLength);

            if (status != NTSTATUS.Success)
                throw new Exception($"GetPEBSafe: failed: {Marshal.GetLastWin32Error()}");

            return pbi.PebBaseAddress;
        }

        /// <summary>
        /// Generates assembly stub to quickly return the address of the linear address PEB
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private IntPtr ConstructStub()
        {
            Assembler asm;
            int instructionsLength = 0;
            if (Environment.Is64BitProcess)
            {
                instructionsLength = 11;
                asm = new Assembler(64);
                asm.mov(AssemblerRegisters.rax, AssemblerRegisters.__dword_ptr.gs[0x60]);
                asm.ret();
                /*
                0:  6548:A1 3000000000000000            mov rax, qword ptr gs:[60]
                B:  c3                                  ret 
                */
            }
            else
            {
                instructionsLength = 7;
                asm = new Assembler(32);
                asm.mov(AssemblerRegisters.eax, AssemblerRegisters.__dword_ptr.fs[0x30]);
                asm.ret();
                /*
                0:  64 a1 30 00 00 00       mov    eax,fs:0x30
                6:  c3                      ret 
                */
            }

            using var stream = new MemoryStream(instructionsLength);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);

            return pBuffer;
        }
    }
}
