﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.Utility
{
    public unsafe class NtosKrnl
    {

        /// <summary>
        /// Mimics behaviour of RtlInitUnicodeString
        /// </summary>
        /// <param name="destinationString"></param>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static NTSTATUS MyRtlInitUnicodeString(UNICODE_STRING* destinationString, string sourceString)
        {
            if (string.IsNullOrEmpty(sourceString))
                return NTSTATUS.InvalidParameter;

            int sourceLength = sourceString.Length;
            int bufferSize = (sourceLength + 1) * sizeof(char);

            IntPtr buffer = Marshal.StringToHGlobalUni(sourceString); //Marshal.StringToCoTaskMemUni(sourceString);
            if (buffer == IntPtr.Zero)
                return NTSTATUS.NoMemory;

            // Manually null-terminate the string
            Marshal.WriteInt16(buffer, sourceLength * sizeof(char), 0);
            destinationString->Buffer = buffer;
            destinationString->Length = (ushort)(sourceLength * sizeof(char)); // Length in bytes
            destinationString->MaximumLength = (ushort)bufferSize;

            return NTSTATUS.Success;
        }

        /// <summary>
        /// Mimics behaviour of FreeUnicodeString
        /// </summary>
        /// <param name="unicodeString"></param>
        public static void MyFreeUnicodeString(ref UNICODE_STRING unicodeString)
        {
            if (unicodeString.Buffer != IntPtr.Zero)
            {
                // Free the allocated memory
                Marshal.FreeCoTaskMem(unicodeString.Buffer);
                unicodeString.Buffer = IntPtr.Zero;
                unicodeString.Length = 0;
                unicodeString.MaximumLength = 0;
            }
        }
    }
}
