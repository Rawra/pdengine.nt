﻿using Iced.Intel;
using PDEngine.NT.PInvoke;
using PDEngine.NT.XAnyStructs;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using static Iced.Intel.AssemblerRegisters;
namespace PDEngine.NT.Utility
{
    public unsafe class RDTSC
    {
        /// <summary>
        /// Gets raw result of CPUID opcode
        /// </summary>
#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> GetCPUID;
#else
        public static delegate* unmanaged[Stdcall]<UInt64> GetCPUID;
#endif
        /// <summary>
        /// Gets raw result of RDTSC opcode
        /// The returned value represents the number of CPU clock cycles since the last reset of the counter.
        /// 
        /// In order to have meaningful time values, one needs to convert the clock cycles to time.
        /// This requires knowing the cpu frequency.
        /// 
        /// Formula: S = 1 / (C * F)
        /// S is the time taken for one cycle in seconds (s)
        /// C is the number of cycles
        /// F is the frequency in Hertz (Hz)
        /// 
        /// NOTE:
        /// The instruction can execute out-of-order on modern CPUs due to speculative execution and instruction reordering.
        /// This means the value returned by RDTSC might not correspond to the exact point in the program's execution where it is invoked.
        /// If instructions around RDTSC are reordered, this can lead to inaccurate or confusing timing results.
        /// 
        /// Does not provide any information about the processor core or thread that executed the instruction.
        /// On systems where the TSC is not synchronized across cores, calling RDTSC on different cores can produce inconsistent results.
        /// 
        /// Therefore it is highly recommended to use RDTSC(P) where-ever possible, or alternatively: use in combination with cpuid to assure serialized results.
        /// 
        /// Use the Ex variant if the CoreCLR / Mono version this code runs under does not marshal UInt64 return parameters from
        /// native functions correctly (splitted EDX:EAX return for 64 bit wide types)
        /// </summary>
#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> GetRDTSC;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, void> GetRDTSCEx;
#else
        public static delegate* unmanaged[Stdcall]<UInt64> GetRDTSC;
        public static delegate* unmanaged[Stdcall]<UInt32*, UInt32*, void> GetRDTSCEx;
#endif

        /// <summary>
        /// Gets raw result of RDTSCP opcode
        /// 
        /// NOTE:
        /// The instruction is semi serializing, meaning it waits for all previous instructions to complete before reading the TSC 
        /// and prevents subsequent instructions from executing until it finishes.
        /// This ensures that the value returned is more closely tied to the exact program point where RDTSCP is called.
        /// 
        /// Returns a supplementary value (in the ECX register) that contains the processor ID and NUMA node ID on many architectures.
        /// This helps identify the CPU core where the instruction was executed, making it useful for multi-core or multi-processor systems.
        /// 
        /// Use the Ex variant if you have similiar issues on x86 as with RDTSC above, or need to have all values seperately delivered, incl cpu core id.
        /// </summary>
#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> GetRDTSCP;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, UInt32*, void> GetRDTSCPEx;
#else
        public static delegate* unmanaged[Stdcall]<UInt64> GetRDTSCP;
        public static delegate* unmanaged[Stdcall]<UInt32*, UInt32*, UInt32*, void> GetRDTSCPEx;
#endif
        /// <summary>
        /// Gets raw result of a combination of RDTSC and CPUID
        /// </summary>
#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> GetRDTSCANDCPUID;
#else
        public static delegate* unmanaged[Stdcall]<UInt64> GetRDTSCANDCPUID;
#endif

        /// <summary>
        /// Gets the cpu manufacturer name (requires storage to the ascii string of fixed size 12 as argument)
        /// returns a pointer to the used storage buffer
        /// </summary>
#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> GetChipVendorName;
#else
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr> GetChipVendorName;
#endif

        /// <summary>
        /// Uses rdtsc. On non-Intel uses Stopwatch.GetTimestamp.
        /// </summary>
#if NET8_0
        public static readonly delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> Timestamp;
#else
        public static readonly delegate* unmanaged[Stdcall]<UInt64> Timestamp;
#endif

        /// <summary>
        /// Uses rdtscp if present. Otherwise uses cpuid + rdtsc. On 
        /// non-Intel uses Stopwatch.GetTimestamp.
        /// </summary>
#if NET8_0
        public static readonly delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64> TimestampP;
#else
        public static readonly delegate* unmanaged[Stdcall]<UInt64> TimestampP;
#endif

        static RDTSC()
        {
            SystemInfo systemInfo;
            try
            {
                Kernel32HP.Instance.GetNativeSystemInfo(&systemInfo);

                // Make sure we are on a supported architecture
                if (systemInfo.wProcessorArchitecture != PROCESSOR_ARCHITECTRURE.INTEL &&
                    systemInfo.wProcessorArchitecture != PROCESSOR_ARCHITECTRURE.AMD64)
                {
                    // Fallback for ARM/IA64/...
#if NET8_0
                    Timestamp = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)Marshal.GetFunctionPointerForDelegate(Stopwatch.GetTimestamp);
                    TimestampP = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)Marshal.GetFunctionPointerForDelegate(Stopwatch.GetTimestamp);
#else
                    Timestamp = (delegate* unmanaged[Stdcall]<UInt64>)Marshal.GetFunctionPointerForDelegate(Stopwatch.GetTimestamp);
                    TimestampP = (delegate* unmanaged[Stdcall]<UInt64>)Marshal.GetFunctionPointerForDelegate(Stopwatch.GetTimestamp);
#endif
                    return;
                }

                // Construct the assembly stubs that retrieve CPUID, RDTSC, RDTSCP, etc for us.
                if (Environment.Is64BitProcess)
                {
#if NET8_0
                    GetCPUID = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX64CPUID();
                    GetRDTSC = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX64RDTSC();
                    GetRDTSCP = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX64RDTSCP();
                    GetRDTSCANDCPUID = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX64RDTSCANDCPUID();
                    GetChipVendorName = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)ConstructX64CPUID_CPUVendorName();
                    GetRDTSCEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, void>)ConstructX64RDTSCEx();
                    GetRDTSCPEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, UInt32*, void>)ConstructX64RDTSCPEx();
#else
                    GetCPUID = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX64CPUID();
                    GetRDTSC = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX64RDTSC();
                    GetRDTSCP = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX64RDTSCP();
                    GetRDTSCANDCPUID = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX64RDTSCANDCPUID();
                    GetChipVendorName = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)ConstructX64CPUID_CPUVendorName();

                    GetRDTSCEx = (delegate* unmanaged[Stdcall]<UInt32*, UInt32*, void>)ConstructX64RDTSCEx();
                    GetRDTSCPEx = (delegate* unmanaged[Stdcall]<UInt32*, UInt32*, UInt32*, void>)ConstructX64RDTSCPEx();
#endif
                }
                else
                {
#if NET8_0
                    GetCPUID = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX86CPUID();
                    GetRDTSC = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX86RDTSC();
                    GetRDTSCP = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX86RDTSCP();
                    GetRDTSCANDCPUID = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt64>)ConstructX86RDTSCANDCPUID();
                    GetChipVendorName = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)ConstructX86CPUID_CPUVendorName();
                    GetRDTSCEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, void>)ConstructX86RDTSCEx();
                    GetRDTSCPEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32*, UInt32*, UInt32*, void>)ConstructX86RDTSCPEx();
#else
                    GetCPUID = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX86CPUID();
                    GetRDTSC = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX86RDTSC();
                    GetRDTSCP = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX86RDTSCP();
                    GetRDTSCANDCPUID = (delegate* unmanaged[Stdcall]<UInt64>)ConstructX86RDTSCANDCPUID();
                    GetChipVendorName = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)ConstructX86CPUID_CPUVendorName();
                    GetRDTSCEx = (delegate* unmanaged[Stdcall]<UInt32*, UInt32*, void>)ConstructX86RDTSCEx();
                    GetRDTSCPEx = (delegate* unmanaged[Stdcall]<UInt32*, UInt32*, UInt32*, void>)ConstructX86RDTSCPEx();
#endif
                }

                // Setup user friendly function
                if (IsRDTSCPSupported())
                {
                    Timestamp = GetRDTSC;
                    TimestampP = GetRDTSCP;
                } 
                else
                {
                    Timestamp = GetRDTSC;
                    TimestampP = GetRDTSCANDCPUID;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error during RDTSC Management class static initialization: {ex.ToString()}");
                Dispose();
                throw;
            } 
        }

        /// <summary>
        /// Function to check if RDTSCP is supported
        /// </summary>
        /// <returns>Supported</returns>
        public static bool IsRDTSCPSupported()
        {
            // We use cpuid, EAX=0x80000001 to check for the rdtscp
            ulong supportedFeatures = GetCPUID();
            if ((supportedFeatures & (1L << 27)) != 0)
            {
                // rdtscp supported
                return true;
            }
            // rdtscp not supported. We use cpuid + rdtsc
            return false;
        }

        /// <summary>
        /// Function to retrieve the CPU Manufacturerer Name
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64CPUID_CPUVendorName()
        {
            Assembler asm = new Assembler(64);
            asm.mov(r8, rcx);
            asm.push(rbx);  // -- save non-volatile register
            asm.xor(rax, rax);
            asm.cpuid();
            // EBX, EDX, ECX
            asm.mov(__qword_ptr[r8 + 0], ebx);
            asm.mov(__qword_ptr[r8 + 4], edx);
            asm.mov(__qword_ptr[r8 + 8], ecx);
            asm.pop(rbx);    // -- restore non-volatile register
            asm.mov(rax, r8);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// CPUID x64
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64CPUID()
        {
            Assembler asm = new Assembler(64);
            Label lbl_end = asm.CreateLabel();
            Label lbl_error = asm.CreateLabel();
            asm.push(rbx);
            asm.mov(eax, 0x80000000);
            asm.cpuid();
            asm.mov(ebx, 0x80000001);
            asm.cmp(eax, ebx);
            asm.jb(lbl_error);
            asm.mov(eax, ebx);
            asm.cpuid();
            asm.mov(eax, ecx);
            asm.shl(rax, 0x20);
            asm.or(rax, rdx);
            asm.jmp(lbl_end);
            // error case
            asm.Label(ref lbl_error);
            asm.xor(rax, rax);
            // end case
            asm.Label(ref lbl_end);
            asm.pop(rbx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSC x64
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64RDTSC()
        {
            Assembler asm = new Assembler(64);
            asm.rdtsc();
            asm.shl(rdx, 0x20); // The SHL (Shift Left) instruction shifts the value in RDX (upper 32 bits of the TSC) left by 32 bits (0x20 in hexadecimal).
            asm.or(rax, rdx);   // ^This moves the upper 32 bits to their proper position in a 64-bit value, making room for the lower 32 bits.
            asm.ret();          // The OR instruction combines the shifted value in RDX (upper 32 bits) with the value in RAX (lower 32 bits).

            //RAX = RDX | EAX
            //    = 0x1234567800000000 | 0x000000009ABCDEF0
            //    = 0x123456789ABCDEF0

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }


        /// <summary>
        /// RDTSCEx x64
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64RDTSCEx()
        {
            Assembler asm = new Assembler(64);
            asm.push(rdx);
            asm.rdtsc();
            asm.mov(__dword_ptr[rcx], eax);
            asm.mov(eax, edx);
            asm.pop(rdx);
            asm.mov(__dword_ptr[rdx], edx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSCPEx x64
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64RDTSCPEx()
        {
            Assembler asm = new Assembler(64);
            // RCX, RDX, R8
            asm.mov(r9, rcx);
            asm.mov(r10, rdx);

            asm.rdtscp();    // EDX:EAX, ECX 
            asm.mov(__qword_ptr[r8], ecx);
            asm.mov(__qword_ptr[r9], eax);
            asm.mov(__qword_ptr[r10], edx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSCP x64
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX64RDTSCP()
        {
            Assembler asm = new Assembler(64);
            asm.rdtscp();
            asm.shl(rdx, 0x20);
            asm.or(rax, rdx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer; 
        }

        /// <summary>
        /// RDTSC + CPUID x64
        /// </summary>
        private static IntPtr ConstructX64RDTSCANDCPUID()
        {
            Assembler asm = new Assembler(64);
            asm.push(rbx);
            asm.xor(eax, eax);
            asm.cpuid();
            asm.rdtsc();
            asm.shl(rdx, 0x20);
            asm.or(rax, rdx);
            asm.pop(rbx);
            asm.ret();

            using var cpuidStream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(cpuidStream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)cpuidStream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(cpuidStream.GetBuffer(), 0, pBuffer, (int)cpuidStream.Length);
            return pBuffer;
        }


        /// <summary>
        /// Function to retrieve the CPU Manufacturerer Name
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX86CPUID_CPUVendorName()
        {
            Assembler asm = new Assembler(32);

            asm.push(ebx);      // -- non-volatile register
            //asm.push(edx);    // -- volatile register
            //asm.push(ecx);    // -- volatile register

            asm.push(ebp);
            asm.mov(ebp, ecx);

            asm.mov(eax, 0);
            asm.cpuid();

            // EBX, EDX, ECX
            asm.mov(__dword_ptr[ebp + 0], ebx);
            asm.mov(__dword_ptr[ebp + 4], edx);
            asm.mov(__dword_ptr[ebp + 8], ecx);
            asm.mov(eax, ebp);
            // CPU Manufacturer string format=AAAAAAAAAAAA

            asm.pop(ebp);

            //asm.pop(ecx);    // -- volatile register
            //asm.pop(edx);    // -- volatile register
            asm.pop(ebx);       // -- non-volatile register
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// CPUID x86
        /// </summary>
        private static IntPtr ConstructX86CPUID()
        {
            Assembler asm = new Assembler(32);
            Label lbl_end = asm.CreateLabel();
            Label lbl_error = asm.CreateLabel();
            asm.push(ebx);
            asm.mov(eax, 0x80000000);
            asm.cpuid();
            asm.mov(ebx, 0x80000001);
            asm.cmp(eax, ebx);
            asm.jb(lbl_error);
            asm.mov(eax, ebx);
            asm.cpuid();
            asm.mov(eax, edx);
            asm.mov(edx, ecx);
            asm.jmp(lbl_end);
            asm.Label(ref lbl_error);
            asm.xor(eax, eax);
            asm.xor(edx, edx);
            asm.Label(ref lbl_end);
            asm.pop(ebx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSC x86
        /// The EDX register is loaded with the high-order 32 bits of the MSR and the EAX register is loaded with the low-order 32 bits
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX86RDTSCEx()
        {
            Assembler asm = new Assembler(32);

            asm.rdtsc();
            asm.mov(ecx, __dword_ptr[esp + 4]);
            asm.mov(__dword_ptr[ecx], eax);

            asm.mov(ecx, __dword_ptr[esp + 8]);
            asm.mov(__dword_ptr[ecx], edx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSC x86
        /// The EDX register is loaded with the high-order 32 bits of the MSR and the EAX register is loaded with the low-order 32 bits
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX86RDTSC()
        {
            Assembler asm = new Assembler(32);

            asm.rdtsc();
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSCP x86
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX86RDTSCP()
        {
            Assembler asm = new Assembler(32);
            asm.rdtscp();
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSCP x86
        /// </summary>
        /// <exception cref="Exception"></exception>
        private static IntPtr ConstructX86RDTSCPEx()
        {
            Assembler asm = new Assembler(32);

            asm.rdtscp();
            asm.push(ecx);
            asm.mov(ecx, __dword_ptr[esp + 8]);
            asm.mov(__dword_ptr[ecx], eax);

            asm.mov(ecx, __dword_ptr[esp + 12]);
            asm.mov(__dword_ptr[ecx], edx);
            asm.pop(ecx);

            asm.mov(edx, __dword_ptr[esp + 12]);
            asm.mov(__dword_ptr[edx], ecx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        /// <summary>
        /// RDTSC + CPUID x86
        /// </summary>
        private static IntPtr ConstructX86RDTSCANDCPUID()
        {
            Assembler asm = new Assembler(32);
            asm.push(ebx);
            asm.xor(eax, eax);
            asm.cpuid();
            asm.rdtsc();
            asm.pop(ebx);
            asm.ret();

            using var stream = new MemoryStream(asm.Instructions.Count * IntPtr.Size);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);
            return pBuffer;
        }

        public static void Dispose()
        {
            if (GetCPUID != null)
            {
                if (!Kernel32HP.Instance.VirtualFree((IntPtr)GetCPUID, UIntPtr.Zero, Kernel32.MEM_RELEASE))
                    throw new Exception("VirtualFree Failed to release memory for: CPUID");
            }
            if (GetRDTSC != null)
            {
                if (!Kernel32HP.Instance.VirtualFree((IntPtr)GetRDTSC, UIntPtr.Zero, Kernel32.MEM_RELEASE))
                    throw new Exception("VirtualFree Failed to release memory for: RDTSC");
            }
            if (GetRDTSCP != null)
            {
                if (!Kernel32HP.Instance.VirtualFree((IntPtr)GetRDTSCP, UIntPtr.Zero, Kernel32.MEM_RELEASE))
                    throw new Exception("VirtualFree Failed to release memory for: RDTSCP");
            }
            if (GetRDTSCANDCPUID != null)
            {
                if (!Kernel32HP.Instance.VirtualFree((IntPtr)GetRDTSCANDCPUID, UIntPtr.Zero, Kernel32.MEM_RELEASE))
                    throw new Exception("VirtualFree Failed to release memory for: RDTSCANDCPUID");
            }
        }
    }
}
