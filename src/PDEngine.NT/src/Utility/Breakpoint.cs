﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Iced.Intel;
using PDEngine.NT.PInvoke;
using PDEngine.NT.XAnyStructs;

namespace PDEngine.NT.Utility
{
    public unsafe class Breakpoint : IDisposable
    {
        private static readonly Lazy<Breakpoint> lazy = new Lazy<Breakpoint>(() => new Breakpoint());

        public static Breakpoint Instance { get { return lazy.Value; } }

        private Breakpoint()
        {
            _stubAddress = ConstructStub();
            GetAddress = (delegate* unmanaged[Stdcall]<void>)_stubAddress;
        }

        /// <summary>
        /// Address of the stub with the generated assembly code
        /// </summary>
        private IntPtr _stubAddress = IntPtr.Zero;

        /// <summary>
        /// Retrieve the address of the PEB
        /// </summary>
        public delegate* unmanaged[Stdcall]<void> GetAddress;
        public static void Break() => Instance.GetAddress();

        /// <summary>
        /// Never call this and try to access GetAddress()
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void Dispose()
        {
            if (_stubAddress == IntPtr.Zero)
                return;

            // Free the allocated memory to restore the previous state (dwSize 0 due to MEM_RELEASE)
            if (!Kernel32.VirtualFree(_stubAddress, 0, Kernel32.MEM_RELEASE))
                throw new Exception("VirtualFree Failed to release memory");
        }

        /// <summary>
        /// Generates assembly stub to quickly break via INT3
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private IntPtr ConstructStub()
        {
            Assembler asm;
            int instructionsLength = 0;
            if (Environment.Is64BitProcess)
            {
                instructionsLength = 3;
                asm = new Assembler(64);
                asm.int3();
                asm.ret();
            }
            else
            {
                instructionsLength = 3;
                asm = new Assembler(32);
                asm.int3();
                asm.ret();
            }

            using var stream = new MemoryStream(instructionsLength);
            if (!asm.TryAssemble(new StreamCodeWriter(stream), 0, out string? errorMessage, out AssemblerResult assemblerResult))
            {
                throw new Exception($"Iced assemble failed: {errorMessage}");
            }

            // Allocate memory with execute-read-write permissions
            nint pBuffer = Kernel32.VirtualAlloc(0, (uint)stream.Length, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
            if (pBuffer == 0)
                throw new Exception("VirtualAlloc Failed to allocate memory");
            Marshal.Copy(stream.GetBuffer(), 0, pBuffer, (int)stream.Length);

            return pBuffer;
        }
    }
}
