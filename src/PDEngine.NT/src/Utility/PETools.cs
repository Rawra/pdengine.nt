﻿using PDEngine.NT.Managers;
using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PDEngine.NT.Utility
{
    public class PETools
    {
        /// <summary>
        /// Try to get a section segment address of a given module.
        /// </summary>
        /// <param name="sectionName">The section name to seek</param>
        /// <param name="sectionVA">The section address found</param>
        /// <returns>Success</returns>
        public unsafe static bool TryGetModuleSectionAddress(IntPtr imageBase, string sectionName, out IntPtr sectionVA)
        {
            sectionVA = IntPtr.Zero;

            // Read the DOS header
            IMAGE_DOS_HEADER dosHeader = new IMAGE_DOS_HEADER();

            if (!MemoryManager.Read<IMAGE_DOS_HEADER>(imageBase, out dosHeader))
            {
                Debug.WriteLine("Could not read DOS header.");
                return false;
            }

            // Read the NT headers
            IntPtr ntHeadersAddress = IntPtr.Add(imageBase, dosHeader.e_lfanew);
            MemoryManager.Read(ntHeadersAddress, out uint signature);
            if (signature != 0x00004550) // "PE\0\0" signature
            {
                Debug.WriteLine("Invalid PE signature.");
                return false;
            }

            IMAGE_FILE_HEADER fileHeader = new IMAGE_FILE_HEADER();
            if (!MemoryManager.Read(IntPtr.Add(ntHeadersAddress, 4), out fileHeader))
            {
                Debug.WriteLine("Could not read file header.");
                return false;
            }

            // Read the appropriate section header
            IntPtr sectionHeadersAddress;
            uint numberOfSections;
            if (fileHeader.SizeOfOptionalHeader == Marshal.SizeOf(typeof(IMAGE_OPTIONAL_HEADER32)))
            {
                IMAGE_NT_HEADERS32 ntHeaders = new IMAGE_NT_HEADERS32();
                if (!MemoryManager.Read(ntHeadersAddress, out ntHeaders))
                {
                    Debug.WriteLine("Could not read NT headers.");
                    return false;
                }
                sectionHeadersAddress = IntPtr.Add(ntHeadersAddress, Marshal.SizeOf(typeof(IMAGE_NT_HEADERS32)));
                numberOfSections = ntHeaders.FileHeader.NumberOfSections;
            }
            else if (fileHeader.SizeOfOptionalHeader == Marshal.SizeOf(typeof(IMAGE_OPTIONAL_HEADER64)))
            {
                IMAGE_NT_HEADERS64 ntHeaders = new IMAGE_NT_HEADERS64();
                if (!MemoryManager.Read(ntHeadersAddress, out ntHeaders))
                {
                    Debug.WriteLine("Could not read NT headers.");
                    return false;
                }
                sectionHeadersAddress = IntPtr.Add(ntHeadersAddress, Marshal.SizeOf(typeof(IMAGE_NT_HEADERS64)));
                numberOfSections = ntHeaders.FileHeader.NumberOfSections;
            }
            else
            {
                Debug.WriteLine("Unknown optional header size.");
                return false;
            }

            // Iterate over the section headers to find the specified section
            for (int i = 0; i < numberOfSections; i++)
            {
                IMAGE_SECTION_HEADER sectionHeader = new IMAGE_SECTION_HEADER();
                if (!MemoryManager.Read(IntPtr.Add(sectionHeadersAddress, i * Marshal.SizeOf(typeof(IMAGE_SECTION_HEADER))), out sectionHeader))
                {
                    Debug.WriteLine($"Could not read section header at {sectionHeadersAddress.ToString("X8")}");
                    return false;
                }

                string currentSectionName = sectionHeader.GetName();
                if (currentSectionName == sectionName)
                {
                    sectionVA = (IntPtr)sectionHeader.VirtualAddress;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if a given pe file is build for x86_64 or not.
        /// </summary>
        /// <param name="peFilePath">The PE file in question</param>
        /// <returns>Is 64 bit</returns>
        public unsafe static bool IsPE64Bit(string peFilePath)
        {
            if (!File.Exists(peFilePath))
            {
                Debug.WriteLine($"Error while trying to determine 64bitness of pe file [{peFilePath}]: file does not exist");
                return false;
            }

            try
            {
                byte[] peFileBytes = File.ReadAllBytes(peFilePath);
                fixed (byte* ptr = peFileBytes)
                {
                    IMAGE_DOS_HEADER* dosHeader = (IMAGE_DOS_HEADER*)ptr;
                    IMAGE_NT_HEADERS32* ntHeader32 = (IMAGE_NT_HEADERS32*)(ptr + dosHeader->e_lfanew);
                    return ntHeader32->FileHeader.Machine == IMAGE_FILE_MACHINE.AMD64;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error while trying to determine 64bitness of pe file [{peFilePath}]: {ex.ToString()}");
                return false;
            }
        }

        public unsafe static bool IsPELargeAddressAware(string peFilePath)
        {
            if (!File.Exists(peFilePath))
            {
                Debug.WriteLine($"Error: [{peFilePath}]: file does not exist");
                return false;
            }

            try
            {
                byte[] peFileBytes = File.ReadAllBytes(peFilePath);
                fixed (byte* ptr = peFileBytes)
                {
                    IMAGE_DOS_HEADER* dosHeader = (IMAGE_DOS_HEADER*)ptr;
                    IMAGE_NT_HEADERS32* ntHeader32 = (IMAGE_NT_HEADERS32*)(ptr + dosHeader->e_lfanew);
                    return (ntHeader32->FileHeader.Characteristics & ImageFileCharacteristics.LargeAddressAware) != 0;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error while trying to determine LAA of pe file [{peFilePath}]: {ex.ToString()}");
                return false;
            }
        }
    }
}
