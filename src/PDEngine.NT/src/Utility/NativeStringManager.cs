﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.Utility
{
    public unsafe sealed class NativeStringManager
    {
        private static readonly Lazy<NativeStringManager> lazy = new Lazy<NativeStringManager>(() => new NativeStringManager());

        public static NativeStringManager Instance { get { return lazy.Value; } }

        private NativeStringManager()
        {
            cachedManagedToNativeStrings = new Dictionary<string, IntPtr>();
        }

        private Dictionary<string, IntPtr> cachedManagedToNativeStrings;

        /// <summary>
        /// Caches all read-in strings, meaning when 2 of the same strings get read, they will only stick around
        /// in one memory location and no duplication will occur.
        /// </summary>
        /// <param name="str">The managed string to marshal to a unmanaged one</param>
        /// <returns>Unmanaged string address</returns>
        public IntPtr GetNativeString(string str)
        {
            if (!cachedManagedToNativeStrings.TryGetValue(str, out IntPtr strOldPtr)) 
            {
                IntPtr strNewPtr = Marshal.StringToHGlobalAnsi(str);
                cachedManagedToNativeStrings.Add(str, strNewPtr);
                return strNewPtr;
            }
            return strOldPtr;
        }
    }
}
