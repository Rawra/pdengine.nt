﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PDEngine.NT.Utility
{
    public class StringTools
    {
        /// <summary>
        /// .NET 4 backport of Marshal.PtrToStringUTF8
        /// Can be used to read UNICODE_STRING structures
        /// </summary>
        /// <param name="ptr"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string PtrToStringUTF8_NET4(nint ptr, int length)
        {
            if (ptr == 0)
                return string.Empty;

            // Copy the bytes into a managed byte array
            byte[] buffer = new byte[length];
            Marshal.Copy(ptr, buffer, 0, length);

            // Convert the byte array to a UTF-8 string
            return Encoding.UTF8.GetString(buffer);
        }

        /// <summary>
        /// UnicodeString to String helper function (IntPtr overload)
        /// </summary>
        /// <param name="pUnicodeString">The unicode string</param>
        /// <returns>The managed string</returns>
        public unsafe static string UnicodeStringToString(IntPtr pUnicodeString) => UnicodeStringToString((UNICODE_STRING*)pUnicodeString);

        /// <summary>
        /// UnicodeString to String helper function
        /// </summary>
        /// <param name="unicodeString">The unicode string</param>
        /// <returns>The managed string</returns>
        public unsafe static string UnicodeStringToString(UNICODE_STRING* unicodeString)
        {
            string value = new string((char*)unicodeString->Buffer, 0, unicodeString->Length / 2);
            value = value.Replace("\0", string.Empty);
            return value;
        }

        /// <summary>
        /// Read a zero terminated string's length (zero based!)
        /// </summary>
        /// <param name="ptr">The zero terminated string start</param>
        /// <returns>The string length</returns>
        public unsafe static uint ReadZeroTerminatedStringLength(byte* ptr)
        {
            uint length = 0;
            while (*ptr++ != 0)
                length++;

            return length;
        }

        /// <summary>
        /// Reads a zero terminated string as a ReadOnlySpan
        /// </summary>
        /// <param name="ptr">The zero terminated string start</param>
        /// <returns>The span containing the zts</returns>
        public unsafe static ReadOnlySpan<byte> ReadZeroTerminatedAsSpan(byte* ptr)
        {
            uint length = ReadZeroTerminatedStringLength(ptr);
            return new ReadOnlySpan<byte>(ptr, (int)length);
        }

        /// <summary>
        /// Reads a zero terminated string as a managed string (IntPtr overload)
        /// </summary>
        /// <param name="ptr">The zero terminated string start</param>
        /// <returns>The managed string</returns>
        public unsafe static string ReadZeroTerminatedString(IntPtr ptr) => ReadZeroTerminatedString((byte*)ptr);

        /// <summary>
        /// Reads a zero terminated string as a managed string
        /// </summary>
        /// <param name="ptr">The zero terminated string start</param>
        /// <returns>The managed string</returns>
        public unsafe static string ReadZeroTerminatedString(byte* ptr)
        {
            uint length = ReadZeroTerminatedStringLength(ptr);
            return Encoding.Default.GetString(ptr, (int)length);
        }
    }
}
