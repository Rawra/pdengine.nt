﻿using PDEngine.NT.Managers;
using PDEngine.NT.Memory;
using Reloaded.Memory.Sigscan;
using System;
namespace PDEngine.NT.PInvokeUnmapped
{
    /// <summary>
    /// This file specializes in listing functions are not exported via EAT
    /// The reasons for which are numerous, so please use with caution.
    /// </summary>
    public class NtDllEx
    {
        private static readonly Lazy<NtDllEx> lazy = new Lazy<NtDllEx>(() => new NtDllEx());

        public static NtDllEx Instance { get { return lazy.Value; } }

        private unsafe NtDllEx()
        {
            IntPtr ntdllImageBase = IntPtr.Zero;
            uint ntdllSizeOfImage = 0;
            if (Environment.Is64BitProcess)
            {
                var ntdll64 = PDProcessExplorer64.Instance.GetLDRInMemoryOrderLoadedEntryByFileName("ntdll.dll");
                ntdllImageBase = ntdll64->DllBase;
                ntdllSizeOfImage = ntdll64->SizeOfImage;
            } 
            else
            {
                var ntdll32 = PDProcessExplorer32.Instance.GetLDRInMemoryOrderLoadedEntryByFileName("ntdll.dll");
                ntdllImageBase = ntdll32->DllBase;
                ntdllSizeOfImage = ntdll32->SizeOfImage;
            }
            byte* imageBasePtr = (byte*)ntdllImageBase;
            Scanner scanner = new Scanner(imageBasePtr, (int)ntdllSizeOfImage);

            // LdrpLoadDll
            if (!SearchTools.TryScanForPattern(ref scanner, 
                "8B FF 55 8B EC 81 EC 14 01 00 00 A1 ?? ?? ?? ?? 33 C5 89 45 FC 53",
                "40 55 53 56 57 41 56 41 57 48 8D AC",
                ntdllImageBase, out LdrpLoadDllAddress))
            {
                throw new Exception("Could not find LdrpLoadDll signature");
            }
        }

        // Function addresses
        public IntPtr LdrpLoadDllAddress;


    }
}
