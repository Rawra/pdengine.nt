﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.X64Structs
{

    /// <summary>
    /// Made for: Microsoft Windows (Version 21H2, Build 22000.2538)
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct _PEB
    {
        public byte InheritedAddressSpace;
        public byte ReadImageFileExecOptions;
        public byte BeingDebugged;
        public byte BitField;
        public fixed byte padding0[4];
        public IntPtr Mutant;
        public IntPtr ImageBaseAddress;
        public _PEB_LDR_DATA* Ldr;
        public IntPtr ProcessParameters;
        public IntPtr SubSystemData;
        public IntPtr ProcessHeap;
        public IntPtr FastPebLock;
        public IntPtr AtlThunkSListPtr;
        public IntPtr IFEOKey;
        public UInt32 CrossProcessFlags;
        public fixed byte padding1[4];
        public IntPtr KernelCallbackTable;
        public UInt32 SystemReserved;
        public UInt32 AtlThunkSListPtr32;
        public IntPtr ApiSetMap;
        public UInt32 TlsExpansionCounter;
        public fixed byte padding2[4];
        public IntPtr TlsBitmap;
        public fixed UInt32 TlsBitmapBits[2];
        public IntPtr ReadOnlySharedMemoryBase;
        public IntPtr SharedData;
        public IntPtr* ReadOnlyStaticServerData;
        public IntPtr AnsiCodePageData;
        public IntPtr OemCodePageData;
        public IntPtr UnicodeCaseTableData;
        public UInt32 NumberOfProcessors;
        public UInt32 NtGlobalFlag;
        public LARGE_INTEGER CriticalSectionTimeout;
        public UInt64 HeapSegmentReserve;
        public UInt64 HeapSegmentCommit;
        public UInt64 HeapDeCommitTotalFreeThreshold;
        public UInt64 HeapDeCommitFreeBlockThreshold;
        public UInt32 NumberOfHeaps;
        public UInt32 MaximumNumberOfHeaps;
        public IntPtr* ProcessHeaps;
        public IntPtr GdiSharedHandleTable;
        public IntPtr ProcessStarterHelper;
        public UInt32 GdiDCAttributeList;
        public IntPtr LoaderLock;
        public UInt32 OSMajorVersion;
        public UInt32 OSMinorVersion;
        public UInt16 OSBuildNumber;
        public UInt16 OSCSDVersion;
        public UInt32 OSPlatformId;
        public UInt32 ImageSubsystem;
        public UInt32 ImageSubsystemMajorVersion;
        public UInt32 ImageSubsystemMinorVersion;
        public UInt64 ActiveProcessAffinityMask;
        public fixed UInt32 GdiHandleBuffer[60];
        public IntPtr PostProcessInitRoutine;
        public IntPtr TlsExpansionBitmap;
        public fixed UInt32 TlsExpansionBitmapBits[32];
        public UInt32 SessionId;
        public ULARGE_INTEGER AppCompatFlags;
        public ULARGE_INTEGER AppCompatFlagsUser;
        public IntPtr pShimData;
        public IntPtr AppCompatInfo;
        public UNICODE_STRING CSDVersion;
        public IntPtr ActivationContextData;
        public IntPtr ProcessAssemblyStorageMap;
        public IntPtr SystemDefaultActivationContextData;
        public IntPtr SystemAssemblyStorageMap;
        public UInt64 MinimumStackCommit;
        // public fixed IntPtr SparePointers[2];
        public IntPtr sparePointers1;
        public IntPtr sparePointers2;
        public IntPtr PatchLoaderData;
        public IntPtr ChpeV2ProcessInfo;
        public UInt32 AppModelFeatureState;
        public fixed UInt32 SpareUlongs[2];
        public UInt16 ActiveCodePage;
        public UInt16 OemCodePage;
        public UInt16 UseCaseMapping;
        public UInt16 UnusedNlsField;
        public IntPtr WerRegistrationData;
        public IntPtr WerShipAssertPtr;
        public IntPtr pContextData;
        public IntPtr pImageHeaderHash;
        public UInt32 TracingFlags;
        public fixed byte padding6[4];
        public UInt64 CsrServerReadOnlySharedMemoryBase;
        public UInt64 TppWorkerpListLock;
        public LIST_ENTRY TppWorkerpList;
        // public fixed uint WaitOnAddressHashTable[128];
        public _PEB_WaitOnAddressHashTable WaitOnAddressHashTable;
        public IntPtr TelemetryCoverageHeader;
        public UInt32 CloudFileFlags;
        public UInt32 CloudFileDiagFlags;
        public sbyte PlaceholderCompatibilityMode;
        public fixed sbyte PlaceholderCompatibilityModeReserved[7];
        public IntPtr LeapSecondData;
        public UInt32 LeapSecondFlags;
        public UInt32 NtGlobalFlag2;
        public UInt64 ExtendedFeatureDisableMask;
    }

    // helper struct for  dt nt!_PEB +0x3a0 WaitOnAddressHashTable : [128] Ptr64 Void
    [StructLayout(LayoutKind.Sequential, Size = 0x400)]
    public struct _PEB_WaitOnAddressHashTable
    {
        public IntPtr Field0;
    }

    /// <summary>
    /// Made for: Microsoft Windows (Version 21H2, Build 22000.2538)
    /// </summary>
    public unsafe struct _TEB
    {
        public NT_TIB NtTib;
        public IntPtr EnvironmentPointer;
        public CLIENT_ID ClientId;
        public IntPtr ActiveRpcHandle;
        public IntPtr ThreadLocalStoragePointer;
        public _PEB* ProcessEnvironmentBlock;
        public UInt32 LastErrorValue;
        public UInt32 CountOfOwnedCriticalSections;
        public IntPtr CsrClientThread;
        public IntPtr Win32ThreadInfo;
        public fixed UInt32 User32Reserved[26];
        public fixed UInt32 UserReserved[5];
        public IntPtr WOW32Reserved;
        public UInt32 CurrentLocale;
        public UInt32 FpSoftwareStatusRegister;
        public _TEB_ReservedForDebuggerInstrumentation ReservedForDebuggerInstrumentation;
        public _TEB_SystemReserved1 SystemReserved1;
        public sbyte PlaceholderCompatibilityMode;
        public byte PlaceholderHydrationAlwaysExplicit;
        public fixed byte PlaceholderReserved[10];
        public UInt32 ProxiedProcessId;
        public ACTIVATION_CONTEXT_STACK ActivationStack;
        public fixed byte WorkingOnBehalfTicket[8];
        public NTSTATUS ExceptionCode;
        public ACTIVATION_CONTEXT_STACK* ActivationContextStackPointer;
        public IntPtr InstrumentationCallbackSp;
        public IntPtr InstrumentationCallbackPreviousPc;
        public IntPtr InstrumentationCallbackPreviousSp;
        public byte InstrumentationCallbackDisabled;
        public byte UnalignedLoadStoreExceptions;
        public fixed byte Padding1[2];
        public GDI_TEB_BATCH GdiTebBatch;
        public CLIENT_ID RealClientId;
        public IntPtr GdiCachedProcessHandle;
        public UInt32 GdiClientPID;
        public UInt32 GdiClientTID;
        public IntPtr GdiThreadLocalInfo;
        public fixed UInt64 Win32ClientInfo[62];
        public _TEB_glDispatchTable glDispatchTable;
        public fixed UInt64 glReserved1[29];
        public IntPtr glReserved2;
        public IntPtr glSectionInfo;
        public IntPtr glSection;
        public IntPtr glTable;
        public IntPtr glCurrentRC;
        public IntPtr glContext;
        public NTSTATUS LastStatusValue;
        public fixed byte Padding2[4];
        public UNICODE_STRING StaticUnicodeString;
        public fixed UInt16 StaticUnicodeBuffer[261];
        public fixed byte Padding3[6];
        public IntPtr DeallocationStack;
        public _TEB_TlsSlots TlsSlots;
        public LIST_ENTRY TlsLinks;
        public IntPtr Vdm;
        public IntPtr ReservedForNtRpc;
        public IntPtr DbgSsReserved1;
        public IntPtr DbgSsReserved2;
        public UInt32 HardErrorMode;
        public fixed byte Padding4[4];
        public _TEB_Instrumentation Instrumentation;
        public Guid ActivityId;
        public IntPtr SubProcessTag;
        public IntPtr PerflibData;
        public IntPtr EtwTraceData;
        public IntPtr WinSockData;
        public UInt32 GdiBatchCount;
        public _TEB_IdealProcessorUnion IdealProcessorUnion;
        public UInt32 GuaranteedStackBytes;
        public fixed byte Padding5[4];
        public IntPtr ReservedForPerf;
        public IntPtr ReservedForOle;
        public UInt32 WaitingOnLoaderLock;
        public fixed byte Padding6[4];
        public IntPtr SavedPriorityState;
        public UInt64 ReservedForCodeCOverage;
        public IntPtr ThreadPoolData;
        public IntPtr* TlsExpansionSlots;
        public IntPtr ChpeV2CpuAreaInfo;
        public IntPtr Unused;
        public UInt32 MuiGeneration;
        public UInt32 IsImpersonating;
        public IntPtr NlsCache;
        public IntPtr pShimData;
        public UInt32 HeapData;
        public fixed byte Padding7[4];
        public IntPtr CurrentTransactionhandle;
        public IntPtr ActiveFrame;
        public IntPtr FlsData;
        public IntPtr PreferredLanguages;
        public IntPtr UserPrefLanguages;
        public UInt32 MuiImpersonation;
        public UInt16 CrossTebFlags;
        public UInt16 SameTebFlags;
        public IntPtr TxnScopeEnterCallback;
        public IntPtr TxnScopeExitCallback;
        public IntPtr TxnScopeContext;
        public UInt32 LockCount;
        public Int32 WowTebOffset;
        public IntPtr ResourceRetValue;
        public IntPtr ReservedForWdf;
        public UInt64 ReservedForCrt;
        public Guid EffectiveContainerId;
        public UInt64 LastSleepCounter;
        public UInt32 SpinCallCount;
        public fixed byte Padding8[4];
        public UInt64 ExtendedFeatureDisableMask;
    }

    // helper structs for  dt nt!_TEB
    [StructLayout(LayoutKind.Sequential, Size = 0x128)]
    public struct _TEB_ReservedForDebuggerInstrumentation
    {
        public IntPtr Field0;
    }
    [StructLayout(LayoutKind.Sequential, Size = 0x240)]
    public struct _TEB_SystemReserved1
    {
        public IntPtr Field0;
    }
    [StructLayout(LayoutKind.Sequential, Size = 0x748)]
    public struct _TEB_glDispatchTable
    {
        public IntPtr Field0;
    }
    [StructLayout(LayoutKind.Sequential, Size = 0x200)]
    public struct _TEB_TlsSlots
    {
        public IntPtr Field0;
    }
    [StructLayout(LayoutKind.Sequential, Size = 0x88)]
    public struct _TEB_Instrumentation
    {
        public IntPtr Field0;
    }
    [StructLayout(LayoutKind.Explicit, Size = 0x04)]
    public struct _TEB_IdealProcessorUnion
    {
        [FieldOffset(0x00)]
        public IntPtr CurrentIdealProcessor;

        [FieldOffset(0x00)]
        public PROCESSOR_NUMBER IdealProcessorValue;

        [FieldOffset(0x00)]
        public byte ReservedPad0;

        [FieldOffset(0x01)]
        public byte ReservedPad1;

        [FieldOffset(0x02)]
        public byte ReservedPad2;

        [FieldOffset(0x03)]
        public byte IdealProcessor;
    }
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct NT_TIB
    {
        public IntPtr ExceptionList;
        public IntPtr StackBase;
        public IntPtr StackLimit;
        public IntPtr SubSystemTib;
        public IntPtr FiberData;
        public UInt32 Version;
        public IntPtr ArbitraryUserPointer;
        public NT_TIB* Self;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PROCESSOR_NUMBER
    {
        public UInt16 Group;
        public byte Number;
        public byte Reserved;
    }


    [StructLayout(LayoutKind.Sequential)]
    public struct CLIENT_ID
    {
        public IntPtr UniqueProcess;
        public IntPtr UniqueThread;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ACTIVATION_CONTEXT_STACK
    {
        public IntPtr ActiveFrame;
        public LIST_ENTRY FrameListCache;
        public UInt32 Flags;
        public UInt32 NextCookieSequenceNumber;
        public UInt32 StackId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct GDI_TEB_BATCH
    {
        public UInt64 Offset;
        public UInt64 HDC;
        public fixed UInt64 Buffer[310];
    }
}
