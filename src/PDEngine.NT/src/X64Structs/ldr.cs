﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.X64Structs
{

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct _PEB_LDR_DATA
    {
        public UInt32 Length;
        [MarshalAs(UnmanagedType.Bool)]
        public bool Initialized;
        public IntPtr SsHandle;
        public LIST_ENTRY InLoadOrderModuleList;
        public LIST_ENTRY InMemoryOrderModuleList;
        public LIST_ENTRY InInitializationOrderModuleList;
        public IntPtr EntryInProgress;
        [MarshalAs(UnmanagedType.Bool)]
        public bool ShutdownInProgress;
        public fixed byte undefined1[4];
        public IntPtr ShutdownThreadId;
    }

    /// <summary>
    /// Made for: Microsoft Windows (Version 21H2, Build 22000.2538)
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct _LDR_DATA_TABLE_ENTRY
    {
        public LIST_ENTRY InLoadOrderLinks;
        public LIST_ENTRY InMemoryOrderLinks;
        public LIST_ENTRY InInitializationOrderLinks;
        public IntPtr DllBase;
        public IntPtr EntryPoint;
        public UInt32 SizeOfImage;
        public fixed byte undefined1[3];
        public UNICODE_STRING FullDllName;
        public UNICODE_STRING BaseDllName;
        public UInt32 Flags; // Used to represent the FlagGroup
        public UInt16 ObsoleteLoadCount;
        public UInt16 TlsIndex;
        public LIST_ENTRY HashLinks;
        public UInt32 TimeDateStamp;
        public IntPtr EntryPointActivationContext;
        public IntPtr Lock;
        public IntPtr DdagNode;
        public LIST_ENTRY NodeModuleLink;
        public IntPtr LoadContext;
        public IntPtr ParentDllBase;
        public IntPtr SwitchBackContext;
        public RTL_BALANCED_NODE BaseAddressIndexNode;
        public RTL_BALANCED_NODE MappingInfoIndexNode;
        public UInt64 OriginalBase;
        public LARGE_INTEGER LoadTime;
        public UInt32 BaseNameHashValue;
        public LDR_DLL_LOAD_REASON LoadReason;
        public UInt32 ImplicitPathOptions;
        public UInt32 ReferenceCount;
        public UInt32 DependentLoadFlags;
        public byte SigningLevel;
        public UInt32 CheckSum;
        public IntPtr ActivePatchImageBase;
        public LDR_HOT_PATCH_STATE HotPatchState;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 16)]
    public unsafe struct LDR_UNKSTRUCT
    {
        public IntPtr pInitNameMaybe;
        public fixed byte Pad0[16];
        public IntPtr Buffer;
        public int Flags;
        public IntPtr pDllName;
        public fixed byte Pad1[84];
        public byte IsInitedMaybe;
        public fixed byte Pad2[3];
    }
}
