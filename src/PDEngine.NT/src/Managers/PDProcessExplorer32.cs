﻿using PDEngine.NT.X86Structs;
using PDEngine.NT.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PDEngine.NT.PInvoke;
using PDEngine.NT.XAnyStructs;

namespace PDEngine.NT.Managers
{
    public sealed unsafe class PDProcessExplorer32
    {
        private static readonly Lazy<PDProcessExplorer32> lazy = new Lazy<PDProcessExplorer32>(() => new PDProcessExplorer32());

        public static PDProcessExplorer32 Instance { get { return lazy.Value; } }

        // Executing Process Info
        public Process Process { get; private set; }

        /// <summary>
        /// Retrieve the current process handle
        /// </summary>
        private nint GetProcessHandle() => Process.Handle;

        /// <summary>
        /// Retrieve the current process id
        /// </summary>
        /// <returns></returns>
        private int GetProcessId() => Process.Id;


        public _PEB* CurrentPEB
        {
            get
            {
                if (currentPEB == null)
                {
                    if (Globals.GlobalOverrides.API_SAFE_MODE)
                        currentPEB = (_PEB*)PEB.GetPEBSafe();
                    else
                        currentPEB = (_PEB*)PEB.Instance.GetAddress();
                }
                return currentPEB;
            }
            private set
            {
                currentPEB = value;
            }
        }
        private _PEB* currentPEB = null;

        public _PEB_LDR_DATA* LDR
        {
            get
            {
                if (ldr == null)
                {
                    ldr = (_PEB_LDR_DATA*)CurrentPEB->Ldr;
                }
                return ldr;
            }
            private set
            {
                ldr = value;
            }
        }
        private _PEB_LDR_DATA* ldr = null;

        public _LDR_DATA_TABLE_ENTRY* LDR_ENTRY
        {
            get
            {
                if (ldr_entry == null)
                {
                    ldr_entry = GetLDRInMemoryOrderLoadedEntryByFileName(Process.ProcessName + ".exe");
                }
                return ldr_entry;
            }
            private set
            {
                ldr_entry = value;
            }
        }
        private _LDR_DATA_TABLE_ENTRY* ldr_entry = null;

        private PDProcessExplorer32()
        {
            Process = Process.GetCurrentProcess();
        }


        /// <summary>
        /// Retrieve the Image Base Address from the PEB
        /// </summary>
        /// <param name="PEBBaseAddress"></param>
        /// <returns></returns>
        public IntPtr GetImageBaseAddress() => CurrentPEB->ImageBaseAddress;

        /// <summary>
        /// Retrieve the Size of Image fromk the PEB LDR
        /// </summary>
        /// <returns></returns>
        public UInt32 GetSizeOfImage() => LDR_ENTRY->SizeOfImage;

        /// <summary>
        /// Load a HModule by file name (Dll Base Address) via PEB/LDR
        /// Reference:  
        /// https://learn.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb_ldr_data
        /// LIST_ENTRY: https://www.codeproject.com/Articles/800404/Understanding-LIST-ENTRY-Lists-and-Its-Importance (See graphic)
        /// Note: Every Flink and Blink points to _LDR_DATA_TABLE_ENTRY.InMemoryOrderLinks and not the start of the struct.
        /// Note2: Only works on x86 binaries, tested on windows 11
        /// Note3: This variant uses basically only offsets and pointers without using any formal structs.
        /// </summary>
        /// <param name="ldr"></param>
        /// <returns></returns>
        public static unsafe nint GetLDRInMemoryOrderLoadedEntryByLDRAndFileName(nint ldr, string hModuleFileName)
        {
            //Console.WriteLine("LDR: 0x" + ldr.ToString("X8"));
            // listEntry = &ldr.InMemoryOrderModuleList
            nint listEntry = ldr + 0x014;

            // nextEntry = listEntry.Flink
            nint nextEntry = Marshal.ReadIntPtr(listEntry + 0x00);
            //Console.WriteLine($"&LDR->InMemoryOrderModuleList.:\t0x{(ldr + 0x014).ToString("X8")}");

            while (nextEntry != listEntry)
            {
                // dataEntry = PLDR_DATA_TABLE_ENTRY
                nint dataEntry = nextEntry - 0x08; // -0x08 due to structure of _LDR_DATA_TABLE_ENTRY (See WinDbg 'dt nt!_LDR_DATA_TABLE_ENTRY' [.InMemoryOrderLinks])
                //Console.WriteLine($"LDRDataEntry..................:\t0x{dataEntry.ToString("X8")}");

                ushort fullDllNameLength = (ushort)Marshal.ReadInt16(dataEntry + 0x024 + 0x00);
                //Console.WriteLine($"LEN: [{fullDllNameLength}]");
                //string hModuleFilePath = Marshal.PtrToStringUTF8(Marshal.ReadIntPtr(dataEntry + 0x024 + 0x04), fullDllNameLength);
                string hModuleFilePath = StringTools.PtrToStringUTF8_NET4(Marshal.ReadIntPtr(dataEntry + 0x024 + 0x04), fullDllNameLength);
                hModuleFilePath = hModuleFilePath.Replace("\0", string.Empty);  // Remove unwanted nulls due to wchar conversion...
                string hModuleFileNameCurrent = Path.GetFileName(hModuleFilePath); // Remove unwanted nulls due to wchar conversion...

                //Console.WriteLine($"CMP: [{hModuleFileNameCurrent.ToLower()}] TO [{hModuleFileName.ToLower()}]");
                //Console.WriteLine(BitConverter.ToString(UTF8Encoding.UTF8.GetBytes(hModuleFileNameCurrent)));
                //Console.WriteLine(BitConverter.ToString(UTF8Encoding.UTF8.GetBytes(hModuleFileName)));
                //return Marshal.ReadIntPtr(dataEntry + 0x018);

                if (Path.GetFileName(hModuleFileNameCurrent).ToLower() == hModuleFileName.ToLower())
                {
                    // dataEntry.DllBase
                    return Marshal.ReadIntPtr(dataEntry + 0x018);
                }

                nextEntry = Marshal.ReadIntPtr(nextEntry + 0x00);
            }
            return 0;
        }

        /// <summary>
        /// Find a hModule entry in the LDR doubly linked list by file name via PEB/LDR
        /// Reference:  
        /// https://learn.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb_ldr_data
        /// LIST_ENTRY: https://www.codeproject.com/Articles/800404/Understanding-LIST-ENTRY-Lists-and-Its-Importance (See graphic)
        /// Note: Every Flink and Blink points to _LDR_DATA_TABLE_ENTRY.InMemoryOrderLinks and not the start of the struct.
        /// Note2: Only works on x86 binaries, tested on windows 11
        /// </summary>
        /// <param name="ldr"></param>
        /// <returns></returns>
        public unsafe _LDR_DATA_TABLE_ENTRY* GetLDRInMemoryOrderLoadedEntryByFileName(string hModuleFileName)
        {
            _LDR_DATA_TABLE_ENTRY* result = default;
            TraverseInMemoryOrderModuleList((_LDR_DATA_TABLE_ENTRY* entry, ref CancellationTokenSource ctx) =>
            {
                string hModuleFileNameCurrent = StringTools.PtrToStringUTF8_NET4(entry->BaseDllName.Buffer, entry->BaseDllName.Length);
                hModuleFileNameCurrent = hModuleFileNameCurrent.Replace("\0", string.Empty);  // Remove unwanted nulls due to wchar conversion...
                //Console.WriteLine($"hModuleFileNameCurrent: {hModuleFileNameCurrent}");
                if (string.Equals(Path.GetFileName(hModuleFileNameCurrent), hModuleFileName, StringComparison.InvariantCultureIgnoreCase))
                {
                    result = entry;
                    ctx.Cancel();
                    return;
                }
            });
            return result;
        }

        public delegate void traverse_delegate(_LDR_DATA_TABLE_ENTRY* entry, ref CancellationTokenSource ctx);
        /// <summary>
        /// Traverse the InMemoryOrderModuleList with the help of the Win32 DoublyLinkedLists
        /// </summary>
        /// <param name="callback"></param>
        public unsafe void TraverseInMemoryOrderModuleList(traverse_delegate callback)
        {
            _PEB_LDR_DATA* _PEB_LDR_DATA = (_PEB_LDR_DATA*)CurrentPEB->Ldr;
            DoublyLinkedListNodeWin32* startNode = (DoublyLinkedListNodeWin32*)&_PEB_LDR_DATA->InMemoryOrderModuleList.Flink;
            CancellationTokenSource ctx = new CancellationTokenSource();

            // We use a checksum to avoid infinite recursion inside the _LIST_ENTRY elements
            uint checksumMarker = 0;
            _LDR_DATA_TABLE_ENTRY* result = default;
            DoublyLinkedListWin32<_LDR_DATA_TABLE_ENTRY>.TraverseFromNode(startNode, (currentNode) =>
            {
                if (checksumMarker == 0)
                    checksumMarker = currentNode->CheckSum;
                else if (checksumMarker == currentNode->CheckSum)
                {
                    ctx.Cancel();
                    return;
                }
                //string hModuleFileNameCurrent = PtrToStringUTF8_Custom(currentNode->BaseDllName.Buffer, currentNode->BaseDllName.Length);
                //hModuleFileNameCurrent = hModuleFileNameCurrent.Replace("\0", string.Empty);  // Remove unwanted nulls due to wchar conversion...

                //Console.WriteLine($"Query _LDR_DATA_TABLE_ENTRY: {new IntPtr(&currentNode->InLoadOrderLinks).ToString("X8")}");
                //Console.WriteLine($"Query Image Base: {currentNode->DllBase}");
                //Console.WriteLine($"Query BaseDllName: {hModuleFileNameCurrent}");
                callback.Invoke(currentNode, ref ctx);
            }, ctx.Token, -(2 * IntPtr.Size));  // Adjust position due to this pointing to the next &_LDR_DATA_TABLE_ENTRY.InMemoryOrderLinks
        }
    }
}
