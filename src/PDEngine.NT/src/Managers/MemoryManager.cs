﻿using PDEngine.NT.PInvoke;
using PDEngine.NT.Utility;
using PDEngine.NT.XAnyStructs.Unix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.Managers
{
    public sealed unsafe class MemoryManager
    {
        /// <summary>
        /// Write to process memory (Multiplatform, Current Process)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to write</param>
        /// <returns>Success</returns>
        public unsafe static bool Write<T>(IntPtr address, T data) where T : unmanaged
        {
            Process process = Process.GetCurrentProcess();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return WriteNT<T>(process.Handle, address, data);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return WriteUnix<T>((uint)process.Id, address, data);
            }
            throw new PlatformNotSupportedException($"Write is not supported for this platform: {RuntimeInformation.OSDescription}");
        }

        /// <summary>
        /// Write to process memory on Windows
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="hProcess">The handle of the process</param>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to write</param>
        /// <returns>Success</returns>
        public unsafe static bool WriteNT<T>(IntPtr hProcess, IntPtr address, T data) where T : unmanaged
        {
            UInt32 lpNumberOfBytesWritten = 0;
            int size = Unsafe.SizeOf<T>(); //StructTools.GetManagedSize(typeof(T));

            if (!Kernel32HP.Instance.WriteProcessMemory(hProcess, address, (byte*)&data, (uint)size, &lpNumberOfBytesWritten).IsTrue())
            {
                Debug.WriteLine($"MemoryManager: Write failed: {Marshal.GetHRForLastWin32Error()}");
                return false;
            }

            if (lpNumberOfBytesWritten != size)
                Debug.WriteLine($"MemoryManager: Write: written data length not identical to given input type size: written={lpNumberOfBytesWritten}, dataSize={size}");

            return true;
        }

        /// <summary>
        /// Read from process memory (Multiplatform, Current Process)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to read into</param>
        /// <returns>Success</returns>
        public unsafe static bool Read<T>(IntPtr address, out T data) where T : unmanaged
        {
            Process process = Process.GetCurrentProcess();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return ReadNT<T>(process.Handle, address, out data);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return ReadUnix<T>((uint)process.Id, address, out data);
            }
            throw new PlatformNotSupportedException($"Read is not supported for this platform: {RuntimeInformation.OSDescription}");
        }

        /// <summary>
        /// Read from process memory on Windows
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="hProcess">The handle of the process</param>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to read into</param>
        /// <returns>Success</returns>
        public unsafe static bool ReadNT<T>(IntPtr hProcess, IntPtr address, out T data) where T : unmanaged
        {
            IntPtr lpNumberOfBytesRead = IntPtr.Zero;
            int size = Unsafe.SizeOf<T>();

            fixed (T* ptr = &data)
            {
                //if (!Kernel32HP.Instance.ReadProcessMemory(hProcess, address, (byte*)ptr, (uint)size, &lpNumberOfBytesRead).IsTrue())
                if (Kernel32.ReadProcessMemory(hProcess, (void*)address, (void*)ptr, (nint)size, &lpNumberOfBytesRead))
                {
                    Debug.WriteLine($"MemoryManager: Read failed {Marshal.GetHRForLastWin32Error()}");
                    return false;
                }
            }

            if ((int)lpNumberOfBytesRead != size)
                Debug.WriteLine($"MemoryManager: Read: read data length not identical to given input type size: read={lpNumberOfBytesRead}, size={size}");

            return true;
        }

        /// <summary>
        /// Read from process memory on Unix
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="processPID">The PID of the process</param>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to read into</param>
        /// <returns>Success</returns>
        public unsafe static bool ReadUnix<T>(UInt32 processPID, IntPtr address, out T data) where T : unmanaged
        {
            int size = Unsafe.SizeOf<T>();
            IntPtr result = IntPtr.Zero - 1;

            fixed (T* ptr = &data)
            {
                IOVec localIo = new IOVec
                {
                    iov_base = ptr,
                    iov_len = size
                };
                IOVec remoteIo = new IOVec
                {
                    iov_base = (byte*)address,
                    iov_len = size
                };
                result = LibCHP.Instance.process_vm_readv(processPID, &localIo, 1, &remoteIo, 1, 0);
            }
            return (int)result != -1;
        }

        /// <summary>
        /// Write to process memory on Unix
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="processPID">The PID of the process</param>
        /// <param name="address">The address inside the process</param>
        /// <param name="data">The data to write</param>
        /// <returns>Success</returns>
        public unsafe static bool WriteUnix<T>(UInt32 processPID, IntPtr address, T value) where T : unmanaged
        {
            int size = Unsafe.SizeOf<T>();
            IOVec localIo = new IOVec
            {
                iov_base = &value,
                iov_len = size
            };
            IOVec remoteIo = new IOVec
            {
                iov_base = (byte*)address,
                iov_len = size
            };

            IntPtr result = LibCHP.Instance.process_vm_writev(processPID, &localIo, 1, &remoteIo, 1, 0);
            return (int)result != -1;
        }


    }
}
