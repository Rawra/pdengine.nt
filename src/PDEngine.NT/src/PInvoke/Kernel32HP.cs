﻿using PDEngine.NT.Managers;
using PDEngine.NT.Utility;
using PDEngine.NT.XAnyStructs;
using PDEngine.NT.XAnyStructs.MinWinAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.PInvoke
{
    /// <summary>
    /// High performance Kernel32 API based on unmanaged delegate function pointers
    /// </summary>
    public unsafe class Kernel32HP
    {
        private static readonly Lazy<Kernel32HP> lazy = new Lazy<Kernel32HP>(() => new Kernel32HP());

        public static Kernel32HP Instance { get { return lazy.Value; } }

        private Kernel32HP()
        {
            if (Environment.Is64BitProcess)
            {
                _libraryHandle = (IntPtr)PDProcessExplorer32.Instance.GetLDRInMemoryOrderLoadedEntryByFileName("kernel32.dll")->DllBase;
            }
            else
            {
                _libraryHandle = (IntPtr)PDProcessExplorer64.Instance.GetLDRInMemoryOrderLoadedEntryByFileName("kernel32.dll")->DllBase;
            }

            if (_libraryHandle == IntPtr.Zero)
                throw new DllNotFoundException("kernel32 library handle is null");

            LoadFunctionPointers();
        }
        private IntPtr _libraryHandle;

        private void LoadFunctionPointers()
        {
#if NET8_0
            WriteProcessMemory = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32>)NativeLibrary.GetExport(_libraryHandle, "WriteProcessMemory");
            ReadProcessMemory = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32>)NativeLibrary.GetExport(_libraryHandle, "ReadProcessMemory");
            VirtualAlloc = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UIntPtr, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "VirtualAlloc");
            VirtualAllocEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UIntPtr, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "VirtualAllocEx");
            VirtualFree = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UIntPtr, UInt32, bool>)NativeLibrary.GetExport(_libraryHandle, "VirtualFree");
            VirtualProtect = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32*, bool>)NativeLibrary.GetExport(_libraryHandle, "VirtualProtect");
            VirtualProtectEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool>)NativeLibrary.GetExport(_libraryHandle, "VirtualProtectEx");

            LoadLibraryA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "LoadLibraryA");
            LoadLibraryW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "LoadLibraryW");
            LoadLibraryExA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "LoadLibraryExA");
            LoadLibraryExW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "LoadLibraryExW");
            FreeLibrary = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32>)NativeLibrary.GetExport(_libraryHandle, "FreeLibrary");
            AddDllDirectory = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "AddDllDirectory");
            SetDllDirectoryA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32>)NativeLibrary.GetExport(_libraryHandle, "SetDllDirectoryA");
            SetDllDirectoryW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32>)NativeLibrary.GetExport(_libraryHandle, "SetDllDirectoryW");
            GetDllDirectoryA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, IntPtr, UInt32>)NativeLibrary.GetExport(_libraryHandle, "GetDllDirectoryA");
            GetDllDirectoryW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, IntPtr, UInt32>)NativeLibrary.GetExport(_libraryHandle, "GetDllDirectoryW");
            OutputDebugStringA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, void>)NativeLibrary.GetExport(_libraryHandle, "OutputDebugStringA");
            OutputDebugStringW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, void>)NativeLibrary.GetExport(_libraryHandle, "OutputDebugStringW");
            GetProcAddress = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "GetProcAddress");
            GetNativeSystemInfo = (delegate* unmanaged[Stdcall, SuppressGCTransition]<SystemInfo*, void>)NativeLibrary.GetExport(_libraryHandle, "GetNativeSystemInfo");
            WaitForDebugEvent = (delegate* unmanaged[Stdcall, SuppressGCTransition]<DEBUG_EVENT*, UInt32, Bool32>)NativeLibrary.GetExport(_libraryHandle, "WaitForDebugEvent");
            RaiseException = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, UInt32, UInt32, IntPtr, void>)NativeLibrary.GetExport(_libraryHandle, "RaiseException");
#else
            WriteProcessMemory = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "WriteProcessMemory");
            ReadProcessMemory = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "ReadProcessMemory");
            VirtualAlloc = (delegate* unmanaged[Stdcall]<IntPtr, UIntPtr , UInt32, UInt32, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "VirtualAlloc");
            VirtualAllocEx = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UIntPtr , UInt32, UInt32, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "VirtualAllocEx");
            VirtualFree = (delegate* unmanaged[Stdcall]<IntPtr, UIntPtr , UInt32, bool>)Kernel32.GetProcAddress(_libraryHandle, "VirtualFree");
            VirtualProtect = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32*, bool>)Kernel32.GetProcAddress(_libraryHandle, "VirtualProtect");
            VirtualProtectEx = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool>)Kernel32.GetProcAddress(_libraryHandle, "VirtualProtectEx");

            LoadLibraryA = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "LoadLibraryA");
            LoadLibraryW = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "LoadLibraryW");
            LoadLibraryExA = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "LoadLibraryExA");
            LoadLibraryExW = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "LoadLibraryExW");
            FreeLibrary = (delegate* unmanaged[Stdcall]<IntPtr, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "FreeLibrary");
            AddDllDirectory = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "AddDllDirectory");
            SetDllDirectoryA = (delegate* unmanaged[Stdcall]<IntPtr, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "SetDllDirectoryA");
            SetDllDirectoryW = (delegate* unmanaged[Stdcall]<IntPtr, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "SetDllDirectoryW");
            GetDllDirectoryA = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, IntPtr, UInt32>)Kernel32.GetProcAddress(_libraryHandle, "GetDllDirectoryA");
            GetDllDirectoryW = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, IntPtr, UInt32>)Kernel32.GetProcAddress(_libraryHandle, "GetDllDirectoryW");
            OutputDebugStringA = (delegate* unmanaged[Stdcall]<IntPtr, void>)Kernel32.GetProcAddress(_libraryHandle, "OutputDebugStringA");
            OutputDebugStringW = (delegate* unmanaged[Stdcall]<IntPtr, void>)Kernel32.GetProcAddress(_libraryHandle, "OutputDebugStringW");
            GetProcAddress = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, IntPtr>)Kernel32.GetProcAddress(_libraryHandle, "GetProcAddress");
            GetNativeSystemInfo = (delegate* unmanaged[Stdcall]<SystemInfo*, void>)Kernel32.GetProcAddress(_libraryHandle, "GetNativeSystemInfo");
            WaitForDebugEvent = (delegate* unmanaged[Stdcall]<DEBUG_EVENT*, UInt32, Bool32>)Kernel32.GetProcAddress(_libraryHandle, "WaitForDebugEvent");
            RaiseException = (delegate* unmanaged[Stdcall]<UInt32, UInt32, UInt32, IntPtr, void>)Kernel32.GetProcAddress(_libraryHandle, "RaiseException");
#endif
        }


#if NET8_0

        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UIntPtr, UInt32, UInt32, IntPtr> VirtualAlloc;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UIntPtr, UInt32, UInt32, IntPtr> VirtualAllocEx;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UIntPtr, UInt32, bool> VirtualFree;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtectEx;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtect;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> LoadLibraryA;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> LoadLibraryW;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExA;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExW;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32> FreeLibrary;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> AddDllDirectory;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32> SetDllDirectoryA;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, Bool32> SetDllDirectoryW;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, IntPtr, UInt32> GetDllDirectoryA;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, IntPtr, UInt32> GetDllDirectoryW;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, void> OutputDebugStringA;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, void> OutputDebugStringW;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32> ReadProcessMemory;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32> WriteProcessMemory;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, IntPtr> GetProcAddress;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<SystemInfo*, void> GetNativeSystemInfo;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<DEBUG_EVENT*, UInt32, Bool32> WaitForDebugEvent;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, UInt32, UInt32, IntPtr, void> RaiseException;
#else

        public delegate* unmanaged[Stdcall]<IntPtr, UIntPtr , UInt32, UInt32, IntPtr> VirtualAlloc;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UIntPtr , UInt32, UInt32, IntPtr> VirtualAllocEx;
        public delegate* unmanaged[Stdcall]<IntPtr, UIntPtr , UInt32, bool> VirtualFree;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtectEx;
        public delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtect;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr> LoadLibraryA;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr> LoadLibraryW;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExA;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExW;
        public delegate* unmanaged[Stdcall]<IntPtr, Bool32> FreeLibrary;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr> AddDllDirectory;
        public delegate* unmanaged[Stdcall]<IntPtr, Bool32> SetDllDirectoryA;
        public delegate* unmanaged[Stdcall]<IntPtr, Bool32> SetDllDirectoryW;
        public delegate* unmanaged[Stdcall]<IntPtr, UInt32, IntPtr, UInt32> GetDllDirectoryA;
        public delegate* unmanaged[Stdcall]<IntPtr, UInt32, IntPtr, UInt32> GetDllDirectoryW;
        public delegate* unmanaged[Stdcall]<IntPtr, void> OutputDebugStringA;
        public delegate* unmanaged[Stdcall]<IntPtr, void> OutputDebugStringW;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32> ReadProcessMemory;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, Bool32> WriteProcessMemory;
        public delegate* unmanaged[Stdcall]<IntPtr, IntPtr, IntPtr> GetProcAddress;
        public delegate* unmanaged[Stdcall]<SystemInfo*, void> GetNativeSystemInfo;
        public delegate* unmanaged[Stdcall]<DEBUG_EVENT*, UInt32, Bool32> WaitForDebugEvent;
        public delegate* unmanaged[Stdcall]<UInt32, UInt32, UInt32, IntPtr, void> RaiseException;
#endif

    }
}
