﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PDEngine.NT.PInvoke
{
    public static class Kernel32
    {
        public const uint PAGE_EXECUTE_READWRITE = 0x40;
        public const uint PAGE_EXECUTE_READ = 0x20;
        public enum MemoryPermissions : UInt32
        {
            PAGE_EXECUTE = 0x10,
            PAGE_EXECUTE_READ = 0x20,
            PAGE_EXECUTE_READWRITE = 0x40,
            PAGE_EXECUTE_WRITECOPY = 0x80,
            PAGE_NOACCESS = 0x01,
            PAGE_READONLY = 0x02,
            PAGE_READWRITE = 0x04,
            PAGE_WRITECOPY = 0x08,
            PAGE_TARGETS_INVALID = 0x40000000,
            PAGE_TARGETS_NO_UPDATE = 0x40000000
        }

        public const uint MEM_COMMIT = 0x1000;
        public const uint MEM_RELEASE = 0x8000;
        public const string KERNELDLL_NAME = "kernel32.dll";

        [DllImport(KERNELDLL_NAME, SetLastError = true)]
        public static extern nint VirtualAlloc(nint lpAddress, uint dwSize, uint flAllocationType, uint flProtect);
        [DllImport(KERNELDLL_NAME, SetLastError = true)]
        public static extern bool VirtualFree(nint lpAddress, uint dwSize, uint dwFreeType);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi)]
        public extern static IntPtr LoadLibraryA(string lpLibFileName);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi)]
        public extern static IntPtr LoadLibraryExW([MarshalAs(UnmanagedType.LPWStr)] string lpFileName, IntPtr hFile, uint dwFlags);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi,  SetLastError = true)]
        public static extern bool FreeLibrary(nint hLibModule);

        [DllImport(KERNELDLL_NAME)]
        public static extern IntPtr AddDllDirectory([MarshalAs(UnmanagedType.LPWStr)] string NewDirectory);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool SetDllDirectory(string lpPathName);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern uint GetDllDirectory(uint nBufferLength, StringBuilder lpBuffer);


        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Auto)]
        public static extern void OutputDebugStringA([MarshalAs(UnmanagedType.LPStr)] string lpOutputString);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Auto)]
        public static extern void OutputDebugStrinW([MarshalAs(UnmanagedType.LPWStr)] string lpOutputString);


        [DllImport(KERNELDLL_NAME, ExactSpelling = true, SetLastError = true)]
        public static extern unsafe bool ReadProcessMemory(
            IntPtr hProcess,
            void* lpBaseAddress,
            void* lpBuffer,
            nint nSize,
            nint* lpNumberOfBytesRead);

        [DllImport(KERNELDLL_NAME, ExactSpelling = true, SetLastError = true)]
        public static extern unsafe bool WriteProcessMemory(
            IntPtr hProcess,
            void* lpBaseAddress,
            void* lpBuffer,
            nuint nSize,
            nuint* lpNumberOfBytesWritten);

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport(KERNELDLL_NAME, ExactSpelling = true)]
        public static extern void GetNativeSystemInfo(out SystemInfo lpSystemInfo);

        [DllImport(KERNELDLL_NAME, SetLastError = true)]
        private static extern void RaiseException(
        uint dwExceptionCode,
        uint dwExceptionFlags,
        uint nNumberOfArguments,
        IntPtr lpArguments);

    }
}
