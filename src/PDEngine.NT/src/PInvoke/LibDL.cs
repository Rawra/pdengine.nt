﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.PInvoke
{
    public class LibDL
    {
        // Constants for dlopen flags
        private const int RTLD_NOW = 2;

        [DllImport("libdl.so.2", EntryPoint = "dlopen", SetLastError = true)]
        public static extern IntPtr dlopen(string fileName, int flags);

        [DllImport("libdl.so.2", EntryPoint = "dlsym", SetLastError = true)]
        public static extern IntPtr dlsym(IntPtr handle, string symbol);

        [DllImport("libdl.so.2", EntryPoint = "dlclose", SetLastError = true)]
        public static extern int dlclose(IntPtr handle);

        [DllImport("libdl.so.2", EntryPoint = "dlerror", SetLastError = true)]
        public static extern IntPtr dlerror();

        /// <summary>
        /// Try to load the LibC library dynamically
        /// </summary>
        /// <returns></returns>
        /// <exception cref="PlatformNotSupportedException"></exception>
        public static bool TryLoadLibc(out IntPtr handle, out string? error)
        {
            handle = IntPtr.Zero;
            error = string.Empty;
            string libcName;

            // Determine the correct libc name based on the platform
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                libcName = "libc.so.6";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                libcName = "libc.dylib";
            }
            else
            {
                throw new PlatformNotSupportedException("Unsupported Unix platform.");
            }

            // Load the library dynamically using dlopen
            handle = dlopen(libcName, RTLD_NOW);

            if (handle == IntPtr.Zero)
            {
                error = $"Failed to load {libcName}: {Marshal.PtrToStringAnsi(dlerror())!}";
                return false;
            }

            return true;
        }
    }
}
