﻿#if X64
using PDEngine.NT.X64Structs;
#else
using PDEngine.NT.X86Structs;
#endif

using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.PInvoke
{
    public static class NtDll
    {

        // Constants for NtQueryInformationProcess
        public const int ProcessBasicInformation = 0;
        public const int ProcessWow64Information = 26;
        public enum NtQueryInformationProcessClass
        {
            ProcessBasicInformation = 0,
            ProcessDebugPort = 7,
            ProcessWow64Information = 26,
            ProcessImageFileName = 27,
            ProcessBreakOnTermination = 29,
            ProcessTelemetryIdInformation = 64,
            ProcessSubsystemInformation = 75
        }



        public const string NTDLL_NAME = "ntdll.dll";

        [DllImport(NTDLL_NAME)]
        public unsafe static extern NTSTATUS NtQueryInformationProcess(
            IntPtr ProcessHandle,
            int ProcessInformationClass,
            void* ProcessInformation,
            int ProcessInformationLength,
            out int ReturnLength
        );

        [DllImport(NTDLL_NAME, CharSet = CharSet.Unicode)]
        public static extern NTSTATUS RtlUnicodeStringToAnsiString(
            ref UNICODE_STRING DestinationString,
            ref UNICODE_STRING SourceString,
            bool AllocateDestinationString);

        [DllImport(NTDLL_NAME, CharSet = CharSet.Unicode)]
        public static extern NTSTATUS RtlFreeUnicodeString(ref UNICODE_STRING UnicodeString);

        [DllImport(NTDLL_NAME, ExactSpelling = true)]
        public static extern unsafe NTSTATUS LdrLoadDll(
            [Optional] char* PathToFile,
            [Optional] uint Flags,
            ref UNICODE_STRING ModuleFileName,
            out IntPtr BaseAddress);

        [DllImport(NTDLL_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern uint LdrSetDllDirectory(ref UNICODE_STRING dir);

        [DllImport(NTDLL_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern uint LdrGetDllDirectory(out UNICODE_STRING dir);

        [DllImport(NTDLL_NAME, CharSet = CharSet.Unicode)]
        public static extern bool RtlCreateUnicodeString(out UNICODE_STRING DestinationString, [MarshalAs(UnmanagedType.LPWStr)] string SourceString);

        [DllImport(NTDLL_NAME)]
        public static extern bool LdrGetProcedureAddressForCaller(in IntPtr moduleHandle, ref UNICODE_STRING pAnsiStringFunctionName, in UInt16 ordinal, out IntPtr functionAddress, in bool bValue, ref IntPtr callbackAddress);


    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PROCESS_BASIC_INFORMATION
    {
        public NTSTATUS ExitStatus; // Exit status of the process
        public IntPtr PebBaseAddress; // Pointer to the process environment block (PEB)
        public UIntPtr AffinityMask; // Processor affinity mask
        public int BasePriority; // Base priority of the process
        public UIntPtr UniqueProcessId; // Unique identifier (PID) of the process
        public UIntPtr InheritedFromUniqueProcessId; // PID of the parent process
    }
}
