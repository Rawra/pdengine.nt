﻿using PDEngine.NT.Managers;
using PDEngine.NT.Utility;
using PDEngine.NT.XAnyStructs.Unix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.PInvoke
{
    /// <summary>
    /// High performance Kernel32 API based on unmanaged delegate function pointers
    /// </summary>
    internal unsafe class LibCHP
    {
        private static readonly Lazy<LibCHP> lazy = new Lazy<LibCHP>(() => new LibCHP());

        public static LibCHP Instance { get { return lazy.Value; } }

        private LibCHP()
        {
            if (!LibDL.TryLoadLibc(out _libraryHandle, out string? error))
            {
                throw new InvalidOperationException(error!);
            }
        }
        private IntPtr _libraryHandle;

        private void LoadFunctionPointers()
        {
#if NET8_0
            process_vm_readv = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "process_vm_readv");
            process_vm_writev = (delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(_libraryHandle, "process_vm_writev");
#else
            process_vm_readv = (delegate* unmanaged[Stdcall]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr>)LibDL.dlsym(_libraryHandle, "process_vm_readv");
            process_vm_writev = (delegate* unmanaged[Stdcall]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr>)LibDL.dlsym(_libraryHandle, "process_vm_writev");
#endif
        }
        /*
        ssize_t process_vm_readv(
                              pid_t pid,
                              const struct iovec *local_iov,
                              unsigned long liovcnt,
                              const struct iovec *remote_iov,
                              unsigned long riovcnt,
                              unsigned long flags);

        ssize_t process_vm_writev(
                              pid_t pid,
                              const struct iovec *local_iov,
                              unsigned long liovcnt,
                              const struct iovec *remote_iov,
                              unsigned long riovcnt,
                              unsigned long flags);
        */
#if NET8_0

        public delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr> process_vm_readv;
        public delegate* unmanaged[Stdcall, SuppressGCTransition]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr> process_vm_writev;
#else

        public delegate* unmanaged[Stdcall]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr> process_vm_readv;
        public delegate* unmanaged[Stdcall]<UInt32, IOVec*, UInt32, IOVec*, UInt32, UInt32, IntPtr> process_vm_writev;
#endif

    }
}
