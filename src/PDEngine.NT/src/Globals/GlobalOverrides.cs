﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.Globals
{
    public static class GlobalOverrides
    {
        /// <summary>
        /// Safe Mode will force most functions
        /// to rely on conventional Windows API calls
        /// to retrieve process information.
        /// 
        /// If this is set to false, it will try to use registers such as FS/GS to
        /// retrieve info such as PEB and or the TEB, which might not be compatible
        /// even when running software such as wine and or proton.
        /// </summary>
        public static bool API_SAFE_MODE = false;
    }
}
