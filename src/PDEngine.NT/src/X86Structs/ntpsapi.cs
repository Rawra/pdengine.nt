﻿using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.X86Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ANSI_STRING
    {
        public ushort Length;
        public ushort MaximumLength;
        public IntPtr Buffer;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct LIST_ENTRY
    {
        public IntPtr Flink;
        public IntPtr Blink;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct _PEB_LDR_DATA
    {
        public uint Length;
        [MarshalAs(UnmanagedType.U1)]
        public bool Initialized;
        public nint SsHandle;
        public LIST_ENTRY InLoadOrderModuleList;
        public LIST_ENTRY InMemoryOrderModuleList;
        public LIST_ENTRY InInitializationOrderModuleList;
        public nint EntryInProgress;
        [MarshalAs(UnmanagedType.U1)]
        public bool ShutdownInProgress;
        public nint ShutdownThreadId;
    }


    [Flags]
    public enum BitFlags : uint
    {
        PackagedBinary = 0x00000001,
        MarkedForRemoval = 0x00000002,
        ImageDll = 0x00000004,
        LoadNotificationsSent = 0x00000008,
        TelemetryEntryProcessed = 0x00000010,
        ProcessStaticImport = 0x00000020,
        InLegacyLists = 0x00000040,
        InIndexes = 0x00000080,
        ShimDll = 0x00000100,
        InExceptionTable = 0x00000200,
        ReservedFlags1 = 0x00000C00,
        LoadInProgress = 0x00001000,
        LoadConfigProcessed = 0x00002000,
        EntryProcessed = 0x00004000,
        ProtectDelayLoad = 0x00008000,
        ReservedFlags3 = 0x00030000,
        DontCallForThreads = 0x00040000,
        ProcessAttachCalled = 0x00080000,
        ProcessAttachFailed = 0x00100000,
        CorDeferredValidate = 0x00200000,
        CorImage = 0x00400000,
        DontRelocate = 0x00800000,
        CorILOnly = 0x01000000,
        ChpeImage = 0x02000000,
        ChpeEmulatorImage = 0x04000000,
        ReservedFlags5 = 0x08000000,
        Redirected = 0x10000000,
        ReservedFlags6 = 0x60000000,
        CompatDatabaseProcessed = 0x80000000
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct _LDR_DATA_TABLE_ENTRY
    {
        public LIST_ENTRY InLoadOrderLinks;
        public LIST_ENTRY InMemoryOrderLinks;
        public LIST_ENTRY InInitializationOrderLinks;
        public nint DllBase;
        public nint EntryPoint;
        public uint SizeOfImage;
        public UNICODE_STRING FullDllName;
        public UNICODE_STRING BaseDllName;
        public uint Flags; // Used to represent the FlagGroup
        public ushort ObsoleteLoadCount;
        public ushort TlsIndex;
        public LIST_ENTRY HashLinks;
        public uint TimeDateStamp;
        public nint EntryPointActivationContext;
        public nint Lock;
        public nint DdagNode;
        public LIST_ENTRY NodeModuleLink;
        public nint LoadContext;
        public nint ParentDllBase;
        public nint SwitchBackContext;
        public RTL_BALANCED_NODE BaseAddressIndexNode;
        public RTL_BALANCED_NODE MappingInfoIndexNode;
        public ulong OriginalBase;
        public LARGE_INTEGER LoadTime;
        public uint BaseNameHashValue;
        public LDR_DLL_LOAD_REASON LoadReason;
        public uint ImplicitPathOptions;
        public uint ReferenceCount;
        public uint DependentLoadFlags;
        public byte SigningLevel;
        public uint CheckSum;
        public nint ActivePatchImageBase;
        public LDR_HOT_PATCH_STATE HotPatchState;
    }
}
