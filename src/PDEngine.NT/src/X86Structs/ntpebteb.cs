﻿using PDEngine.NT.Utility;
using PDEngine.NT.XAnyStructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.X86Structs
{
    public static class TEBConstants
    {
        public const int STATIC_UNICODE_BUFFER_LENGTH = 261;
        public const int WIN32_CLIENT_INFO_LENGTH = 62;
        public const int WIN32_CLIENT_INFO_SPIN_COUNT = 1;
    }

    public static class MiscConstants
    {
        public const int FLS_MAXIMUM_AVAILABLE = 128;
        public const int TLS_MINIMUM_AVAILABLE = 64;
        public const int TLS_EXPANSION_SLOTS = 1024;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct _PEB
    {
        public byte InheritedAddressSpace;
        public byte ReadImageFileExecOptions;
        public byte BeingDebugged;
        public byte BitField;
        public nint Mutant;
        public nint ImageBaseAddress;
        public nint Ldr;
        public nint ProcessParameters;
        public nint SubSystemData;
        public nint ProcessHeap;
        public nint FastPebLock;
        public nint AtlThunkSListPtr;
        public nint IFEOKey;
        public uint CrossProcessFlags;
        public nint KernelCallbackTable;
        public uint SystemReserved;
        public uint AtlThunkSListPtr32;
        public nint ApiSetMap;
        public uint TlsExpansionCounter;
        public nint TlsBitmap;
        public fixed uint TlsBitmapBits[2];
        public nint ReadOnlySharedMemoryBase;
        public nint SharedData;
        public nint* ReadOnlyStaticServerData;
        public nint AnsiCodePageData;
        public nint OemCodePageData;
        public nint UnicodeCaseTableData;
        public uint NumberOfProcessors;
        public uint NtGlobalFlag;
        public ulong CriticalSectionTimeout;
        public nuint HeapSegmentReserve;
        public nuint HeapSegmentCommit;
        public nuint HeapDeCommitTotalFreeThreshold;
        public nuint HeapDeCommitFreeBlockThreshold;
        public uint NumberOfHeaps;
        public uint MaximumNumberOfHeaps;
        public nint* ProcessHeaps;
        public nint GdiSharedHandleTable;
        public nint ProcessStarterHelper;
        public uint GdiDCAttributeList;
        public nint LoaderLock;
        public uint OSMajorVersion;
        public uint OSMinorVersion;
        public ushort OSBuildNumber;
        public ushort OSCSDVersion;
        public uint OSPlatformId;
        public uint ImageSubsystem;
        public uint ImageSubsystemMajorVersion;
        public uint ImageSubsystemMinorVersion;
        public nuint ActiveProcessAffinityMask;
        public fixed uint GdiHandleBuffer[34];
        public nint PostProcessInitRoutine;
        public nint TlsExpansionBitmap;
        public fixed uint TlsExpansionBitmapBits[32];
        public uint SessionId;
        public ulong AppCompatFlags;
        public ulong AppCompatFlagsUser;
        public nint pShimData;
        public nint AppCompatInfo;
        public UNICODE_STRING CSDVersion;
        public nint ActivationContextData;
        public nint ProcessAssemblyStorageMap;
        public nint SystemDefaultActivationContextData;
        public nint SystemAssemblyStorageMap;
        public nuint MinimumStackCommit;
        public fixed uint SparePointers[2];
        public nint PatchLoaderData;
        public nint ChpeV2ProcessInfo;
        public uint AppModelFeatureState;
        public fixed uint SpareUlongs[2];
        public ushort ActiveCodePage;
        public ushort OemCodePage;
        public ushort UseCaseMapping;
        public ushort UnusedNlsField;
        public nint WerRegistrationData;
        public nint WerShipAssertPtr;
        public nint pContextData;
        public nint pImageHeaderHash;
        public uint TracingFlags;
        public ulong CsrServerReadOnlySharedMemoryBase;
        public nint TppWorkerpListLock;
        public LIST_ENTRY TppWorkerpList;
        public fixed uint WaitOnAddressHashTable[128];
        public nint TelemetryCoverageHeader;
        public uint CloudFileFlags;
        public uint CloudFileDiagFlags;
        public sbyte PlaceholderCompatibilityMode;
        public fixed sbyte PlaceholderCompatibilityModeReserved[7];
        public nint LeapSecondData;
        public uint LeapSecondFlags;
        public uint NtGlobalFlag2;
        public ulong ExtendedFeatureDisableMask;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct NT_TIB
    {
        public IntPtr ExceptionList;
        public IntPtr StackBase;
        public IntPtr StackLimit;
        public IntPtr SubSystemTib;
        public IntPtr FiberData;
        public uint Version;
        public IntPtr ArbitraryUserPointer;
        public IntPtr Self;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CLIENT_ID
    {
        public IntPtr UniqueProcess;
        public IntPtr UniqueThread;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ACTIVATION_CONTEXT_STACK
    {
        public IntPtr ActiveFrame;
        public LIST_ENTRY FrameListCache;
        public uint Flags;
        public uint NextCookieSequenceNumber;
        public uint StackId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct GDI_TEB_BATCH
    {
        private uint _offsetAndHasRenderingCommand;
        public uint Offset
        {
            get { return _offsetAndHasRenderingCommand & 0x7FFFFFFF; }
            set { _offsetAndHasRenderingCommand = (value & 0x7FFFFFFF) | (_offsetAndHasRenderingCommand & 0x80000000); }
        }

        public bool HasRenderingCommand
        {
            get { return (_offsetAndHasRenderingCommand & 0x80000000) != 0; }
            set { _offsetAndHasRenderingCommand = (_offsetAndHasRenderingCommand & 0x7FFFFFFF) | (value ? 0x80000000u : 0u); }
        }

        public uint HDC;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 310)]
        public uint[] Buffer;
    }

    public unsafe struct _TEB
    {
        public NT_TIB NtTib;
        public IntPtr EnvironmentPointer;
        public CLIENT_ID ClientId;
        public IntPtr ActiveRpcHandle;
        public IntPtr ThreadLocalStoragePointer;
        public IntPtr ProcessEnvironmentBlock;
        public uint LastErrorValue;
        public uint CountOfOwnedCriticalSections;
        public IntPtr CsrClientThread;
        public IntPtr Win32ThreadInfo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
        public uint[] User32Reserved;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        public uint[] UserReserved;
        public IntPtr WOW32Reserved;
        public uint CurrentLocale;
        public uint FpSoftwareStatusRegister;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public IntPtr[] ReservedForDebuggerInstrumentation;
#if _WIN64
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
#else
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
#endif
        public IntPtr[] SystemReserved1;
        public byte PlaceholderCompatibilityMode;
        public bool PlaceholderHydrationAlwaysExplicit;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public byte[] PlaceholderReserved;
        public uint ProxiedProcessId;
        public ACTIVATION_CONTEXT_STACK ActivationStack;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] WorkingOnBehalfTicket;
        public NTSTATUS ExceptionCode;
        public IntPtr ActivationContextStackPointer;
        public IntPtr InstrumentationCallbackSp;
        public IntPtr InstrumentationCallbackPreviousPc;
        public IntPtr InstrumentationCallbackPreviousSp;
#if _WIN64
    public uint TxFsContext;
#endif
        public bool InstrumentationCallbackDisabled;
#if _WIN64
    public bool UnalignedLoadStoreExceptions;
#else
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 23)]
        public byte[] SpareBytes;
#endif
        public GDI_TEB_BATCH GdiTebBatch;
        public CLIENT_ID RealClientId;
        public IntPtr GdiCachedProcessHandle;
        public uint GdiClientPID;
        public uint GdiClientTID;
        public IntPtr GdiThreadLocalInfo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = TEBConstants.WIN32_CLIENT_INFO_LENGTH)]
        public ulong[] Win32ClientInfo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 233)]
        public IntPtr[] glDispatchTable;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 29)]
        public ulong[] glReserved1;
        public IntPtr glReserved2;
        public IntPtr glSectionInfo;
        public IntPtr glSection;
        public IntPtr glTable;
        public IntPtr glCurrentRC;
        public IntPtr glContext;
        public NTSTATUS LastStatusValue;
        public UNICODE_STRING StaticUnicodeString;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = TEBConstants.STATIC_UNICODE_BUFFER_LENGTH)]
        public char[] StaticUnicodeBuffer;
        public IntPtr DeallocationStack;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = MiscConstants.TLS_MINIMUM_AVAILABLE)]
        public IntPtr[] TlsSlots;
        public LIST_ENTRY TlsLinks;
        public IntPtr Vdm;
        public IntPtr ReservedForNtRpc;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public IntPtr[] DbgSsReserved;
        public uint HardErrorMode;
#if _WIN64
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
#else
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
#endif
        public IntPtr[] Instrumentation;
        public Guid ActivityId;
        public IntPtr SubProcessTag;
        public IntPtr PerflibData;
        public IntPtr EtwTraceData;
        public IntPtr WinSockData;
        public uint GdiBatchCount;
        public ushort CrossTebFlags;
        public ushort SameTebFlags;
        public IntPtr TxnScopeEnterCallback;
        public IntPtr TxnScopeExitCallback;
        public IntPtr TxnScopeContext;
        public uint LockCount;
        public int WowTebOffset;
        public IntPtr ResourceRetValue;
        public IntPtr ReservedForWdf;
        public ulong ReservedForCrt;
        public Guid EffectiveContainerId;
        public ulong LastSleepCounter;
        public uint SpinCallCount;
        public ulong ExtendedFeatureDisableMask;
    }
}
