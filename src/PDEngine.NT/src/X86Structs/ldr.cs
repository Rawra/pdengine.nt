﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.X86Structs
{
    [StructLayout(LayoutKind.Sequential, Pack = 16)]
    public unsafe struct LDR_UNKSTRUCT
    {
        public IntPtr pInitNameMaybe;
        public fixed byte Pad0[16];
        public IntPtr Buffer;
        public int Flags;
        public IntPtr pDllName;
        public fixed byte Pad1[84];
        public byte IsInitedMaybe;
        public fixed byte Pad2[3];
    }
}
