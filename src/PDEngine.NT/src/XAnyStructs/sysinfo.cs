﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SystemInfo
    {
        public PROCESSOR_ARCHITECTRURE wProcessorArchitecture;
        public ushort wReserved;
        public uint dwPageSize;
        public IntPtr lpMinimumApplicationAddress;
        public IntPtr lpMaximumApplicationAddress;
        public IntPtr dwActiveProcessorMask;
        public uint dwNumberOfProcessors;
        public uint dwProcessorType;
        public uint dwAllocationGranularity;
        public ushort wProcessorLevel;
        public ushort wProcessorRevision;
    }

    public enum PROCESSOR_ARCHITECTRURE : ushort
    {
        /// <summary>
        /// x64 (AMD or Intel)
        /// </summary>
        AMD64 = 9,

        /// <summary>
        /// ARM
        /// </summary>
        ARM = 5,

        /// <summary>
        /// ARM64
        /// </summary>
        ARM64 = 12,

        /// <summary>
        /// Intel Itanium-based
        /// </summary>
        IA64 = 6,

        /// <summary>
        /// x86
        /// </summary>
        INTEL = 0,

        /// <summary>
        /// Unknown architecture
        /// </summary>
        UNKNOWN = 0xFFFF
    }
}
