﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs.Unix
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IOVec
    {
        public void* iov_base;
        public int iov_len;
    }


    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PID_t
    {
        public Int32 pid;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct SSIZE_t
    {
        public IntPtr size;
    }
}
