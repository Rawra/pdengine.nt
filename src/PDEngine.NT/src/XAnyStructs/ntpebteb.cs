﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{

    [StructLayout(LayoutKind.Sequential)]
    public struct THREAD_BASIC_INFORMATION
    {
        public IntPtr ExitStatus;
        public IntPtr TebBaseAddress;
        public CLIENT_ID ClientId;         // Unique identifier for the process and thread
        public IntPtr AffinityMask;        // KAFFINITY AffinityMask
        public int Priority;
        public int BasePriority;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CLIENT_ID
    {
        public IntPtr UniqueProcess;       // Process ID
        public IntPtr UniqueThread;        // Thread ID
    }
}
