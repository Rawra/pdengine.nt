﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{
    [Flags]
    public enum DllCharacteristics : ushort
    {
        // Image file characteristics
        IMAGE_FILE_RELOCS_STRIPPED = 0x0001,
        IMAGE_FILE_EXECUTABLE_IMAGE = 0x0002,
        IMAGE_FILE_LINE_NUMS_STRIPPED = 0x0004,
        IMAGE_FILE_LOCAL_SYMS_STRIPPED = 0x0008,
        IMAGE_FILE_AGGRESIVE_WS_TRIM = 0x0010,
        IMAGE_FILE_LARGE_ADDRESS_AWARE = 0x0020,
        IMAGE_FILE_BYTES_REVERSED_LO = 0x0080,
        IMAGE_FILE_32BIT_MACHINE = 0x0100,
        IMAGE_FILE_DEBUG_STRIPPED = 0x0200,
        IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP = 0x0400,
        IMAGE_FILE_NET_RUN_FROM_SWAP = 0x0800,
        IMAGE_FILE_SYSTEM = 0x1000,
        IMAGE_FILE_DLL = 0x2000,
        IMAGE_FILE_UP_SYSTEM_ONLY = 0x4000,
        IMAGE_FILE_BYTES_REVERSED_HI = 0x8000
    }

    [Flags]
    public enum ImageFileCharacteristics : ushort
    {
        /// <summary>
        /// Relocation information was stripped from the file. 
        /// The file must be loaded at its preferred base address.
        /// If the base address is not available, the loader reports an error.
        /// </summary>
        RelocsStripped = 0x0001,

        /// <summary>
        /// The file is executable (there are no unresolved external references).
        /// </summary>
        ExecutableImage = 0x0002,

        /// <summary>
        /// COFF line numbers were stripped from the file.
        /// </summary>
        LineNumsStripped = 0x0004,

        /// <summary>
        /// COFF symbol table entries were stripped from the file.
        /// </summary>
        LocalSymsStripped = 0x0008,

        /// <summary>
        /// Aggressively trim the working set. This value is obsolete.
        /// </summary>
        AggressiveWSTrim = 0x0010,

        /// <summary>
        /// The application can handle addresses larger than 2 GB.
        /// </summary>
        LargeAddressAware = 0x0020,

        /// <summary>
        /// The bytes of the word are reversed. This flag is obsolete.
        /// </summary>
        BytesReversedLo = 0x0080,

        /// <summary>
        /// The computer supports 32-bit words.
        /// </summary>
        Machine32Bit = 0x0100,

        /// <summary>
        /// Debugging information was removed and stored separately in another file.
        /// </summary>
        DebugStripped = 0x0200,

        /// <summary>
        /// If the image is on removable media, copy it to and run it from the swap file.
        /// </summary>
        RemovableRunFromSwap = 0x0400,

        /// <summary>
        /// If the image is on the network, copy it to and run it from the swap file.
        /// </summary>
        NetRunFromSwap = 0x0800,

        /// <summary>
        /// The image is a system file.
        /// </summary>
        System = 0x1000,

        /// <summary>
        /// The image is a DLL file. While it is an executable file, it cannot be run directly.
        /// </summary>
        Dll = 0x2000,

        /// <summary>
        /// The file should be run only on a uniprocessor computer.
        /// </summary>
        UpSystemOnly = 0x4000,

        /// <summary>
        /// The bytes of the word are reversed. This flag is obsolete.
        /// </summary>
        BytesReversedHi = 0x8000
    }


    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_DOS_HEADER
    {
        public ushort e_magic;    // Magic number
        public ushort e_cblp;     // Bytes on last page of file
        public ushort e_cp;       // Pages in file
        public ushort e_crlc;     // Relocations
        public ushort e_cparhdr;  // Size of header in paragraphs
        public ushort e_minalloc; // Minimum extra paragraphs needed
        public ushort e_maxalloc; // Maximum extra paragraphs needed
        public ushort e_ss;       // Initial (relative) SS value
        public ushort e_sp;       // Initial SP value
        public ushort e_csum;     // Checksum
        public ushort e_ip;       // Initial IP value
        public ushort e_cs;       // Initial (relative) CS value
        public ushort e_lfarlc;   // File address of relocation table
        public ushort e_ovno;     // Overlay number
//#if NET8_0
        public fixed ushort e_res1[4];   // Reserved words
//#else
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        /*public byte e_res_1;
        public byte e_res_2;
        public byte e_res_3;
        public byte e_res_4;*/
//#endif
        public ushort e_oemid;    // OEM identifier (for e_oeminfo)
        public ushort e_oeminfo;  // OEM information; e_oemid specific
//#if NET8_0
        public fixed ushort e_res2[10];   // Reserved words
//#else
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        /*public byte e_res2_1;
        public byte e_res2_2;
        public byte e_res2_3;
        public byte e_res2_4;
        public byte e_res2_5;
        public byte e_res2_6;
        public byte e_res2_7;
        public byte e_res2_8;
        public byte e_res2_9;
        public byte e_res2_10;*/
//#endif
        public int e_lfanew;      // File address of new exe header
    }

    public enum IMAGE_FILE_MACHINE : ushort
    {
        I386 = 0x014C,
        IA64 = 0x0200,
        AMD64 = 0x8664
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGE_FILE_HEADER
    {
        public IMAGE_FILE_MACHINE Machine;
        public ushort NumberOfSections;
        public uint TimeDateStamp;
        public uint PointerToSymbolTable;
        public uint NumberOfSymbols;
        public ushort SizeOfOptionalHeader;
        public ImageFileCharacteristics Characteristics;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_OPTIONAL_HEADER32
    {
        public ushort Magic;
        public byte MajorLinkerVersion;
        public byte MinorLinkerVersion;
        public uint SizeOfCode;
        public uint SizeOfInitializedData;
        public uint SizeOfUninitializedData;
        public uint AddressOfEntryPoint;
        public uint BaseOfCode;
        public uint BaseOfData;
        public uint ImageBase;
        public uint SectionAlignment;
        public uint FileAlignment;
        public ushort MajorOperatingSystemVersion;
        public ushort MinorOperatingSystemVersion;
        public ushort MajorImageVersion;
        public ushort MinorImageVersion;
        public ushort MajorSubsystemVersion;
        public ushort MinorSubsystemVersion;
        public uint Win32VersionValue;
        public uint SizeOfImage;
        public uint SizeOfHeaders;
        public uint CheckSum;
        public ushort Subsystem;
        public DllCharacteristics DllCharacteristics;
        public uint SizeOfStackReserve;
        public uint SizeOfStackCommit;
        public uint SizeOfHeapReserve;
        public uint SizeOfHeapCommit;
        public uint LoaderFlags;
        public uint NumberOfRvaAndSizes;
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        //public fixed IMAGE_DATA_DIRECTORY DataDirectory[16];

        // FIXME: Number of data directories is not fixed!
        // see: https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_optional_header64
        // specifically: "NumberOfRvaAndSizes" remark
        public IMAGE_DATA_DIRECTORY DataDirectory1;
        public IMAGE_DATA_DIRECTORY DataDirectory2;
        public IMAGE_DATA_DIRECTORY DataDirectory3;
        public IMAGE_DATA_DIRECTORY DataDirectory4;
        public IMAGE_DATA_DIRECTORY DataDirectory5;
        public IMAGE_DATA_DIRECTORY DataDirectory6;
        public IMAGE_DATA_DIRECTORY DataDirectory7;
        public IMAGE_DATA_DIRECTORY DataDirectory8;
        public IMAGE_DATA_DIRECTORY DataDirectory9;
        public IMAGE_DATA_DIRECTORY DataDirectory10;
        public IMAGE_DATA_DIRECTORY DataDirectory11;
        public IMAGE_DATA_DIRECTORY DataDirectory12;
        public IMAGE_DATA_DIRECTORY DataDirectory13;
        public IMAGE_DATA_DIRECTORY DataDirectory14;
        public IMAGE_DATA_DIRECTORY DataDirectory15;
        public IMAGE_DATA_DIRECTORY DataDirectory16;

        public bool TryGetImageDataDirectory(IMAGE_DIRECTORY_ENTRY_CONSTANT entry, out IMAGE_DATA_DIRECTORY* imageDataDirectory)
        {
            int entryIndex = (int)entry;

            // Check if the entry index is valid
            if (entryIndex >= 0 && entryIndex < NumberOfRvaAndSizes)
            {
                fixed (IMAGE_DATA_DIRECTORY* ptr = &DataDirectory1)
                {
                    imageDataDirectory = &(ptr[entryIndex]);
                }
                return true;
            }

            // Invalid index
            imageDataDirectory = default;
            return false;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_OPTIONAL_HEADER64
    {
        public ushort Magic;
        public byte MajorLinkerVersion;
        public byte MinorLinkerVersion;
        public uint SizeOfCode;
        public uint SizeOfInitializedData;
        public uint SizeOfUninitializedData;
        public uint AddressOfEntryPoint;
        public uint BaseOfCode;
        public ulong ImageBase;
        public uint SectionAlignment;
        public uint FileAlignment;
        public ushort MajorOperatingSystemVersion;
        public ushort MinorOperatingSystemVersion;
        public ushort MajorImageVersion;
        public ushort MinorImageVersion;
        public ushort MajorSubsystemVersion;
        public ushort MinorSubsystemVersion;
        public uint Win32VersionValue;
        public uint SizeOfImage;
        public uint SizeOfHeaders;
        public uint CheckSum;
        public ushort Subsystem;
        public DllCharacteristics DllCharacteristics;
        public ulong SizeOfStackReserve;
        public ulong SizeOfStackCommit;
        public ulong SizeOfHeapReserve;
        public ulong SizeOfHeapCommit;
        public uint LoaderFlags;
        public uint NumberOfRvaAndSizes;
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        //public IMAGE_DATA_DIRECTORY[] DataDirectory;

        // FIXME: Number of data directories is not fixed!
        // see: https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_optional_header64
        // specifically: "NumberOfRvaAndSizes" remark
        public IMAGE_DATA_DIRECTORY DataDirectory1;
        public IMAGE_DATA_DIRECTORY DataDirectory2;
        public IMAGE_DATA_DIRECTORY DataDirectory3;
        public IMAGE_DATA_DIRECTORY DataDirectory4;
        public IMAGE_DATA_DIRECTORY DataDirectory5;
        public IMAGE_DATA_DIRECTORY DataDirectory6;
        public IMAGE_DATA_DIRECTORY DataDirectory7;
        public IMAGE_DATA_DIRECTORY DataDirectory8;
        public IMAGE_DATA_DIRECTORY DataDirectory9;
        public IMAGE_DATA_DIRECTORY DataDirectory10;
        public IMAGE_DATA_DIRECTORY DataDirectory11;
        public IMAGE_DATA_DIRECTORY DataDirectory12;
        public IMAGE_DATA_DIRECTORY DataDirectory13;
        public IMAGE_DATA_DIRECTORY DataDirectory14;
        public IMAGE_DATA_DIRECTORY DataDirectory15;
        public IMAGE_DATA_DIRECTORY DataDirectory16;

        public bool TryGetImageDataDirectory(IMAGE_DIRECTORY_ENTRY_CONSTANT entry, out IMAGE_DATA_DIRECTORY* imageDataDirectory)
        {
            int entryIndex = (int)entry;

            // Check if the entry index is valid
            if (entryIndex >= 0 && entryIndex < NumberOfRvaAndSizes)
            {
                fixed (IMAGE_DATA_DIRECTORY* ptr = &DataDirectory1)
                {
                    imageDataDirectory = &(ptr[entryIndex]);
                }
                return true;
            }

            // Invalid index
            imageDataDirectory = default;
            return false;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGE_NT_HEADERS32
    {
        public uint Signature;
        public IMAGE_FILE_HEADER FileHeader;
        public IMAGE_OPTIONAL_HEADER32 OptionalHeader;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGE_NT_HEADERS64
    {
        public uint Signature;
        public IMAGE_FILE_HEADER FileHeader;
        public IMAGE_OPTIONAL_HEADER64 OptionalHeader;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_SECTION_HEADER
    {
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
//#if NET8_0
        public fixed byte Name[8];
//#else
        /*public byte Name1;
        public byte Name2;
        public byte Name3;
        public byte Name4;
        public byte Name5;
        public byte Name6;
        public byte Name7;
        public byte Name8;*/
//#endif
        public uint VirtualSize;
        public uint VirtualAddress;
        public uint SizeOfRawData;
        public uint PointerToRawData;
        public uint PointerToRelocations;
        public uint PointerToLinenumbers;
        public ushort NumberOfRelocations;
        public ushort NumberOfLinenumbers;
        public uint Characteristics;

        public unsafe string GetName()
        {
//#if NET8_0
            fixed (byte* ptr = Name)
            {
                return UTF8Encoding.ASCII.GetString(ptr, 8).TrimEnd('\0');
            }
//#else
            /*fixed (byte* ptr = &Name1)
            {
                return UTF8Encoding.ASCII.GetString(ptr, 8).TrimEnd('\0');
            }*/
//#endif
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_DATA_DIRECTORY
    {
        public uint VirtualAddress;
        public uint Size;
    }


    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct IMAGE_EXPORT_DIRECTORY
    {
        public UInt32 Characteristics;
        public UInt32 TimeDataStamp;
        public UInt16 MajorVersion;
        public UInt16 MinorVersion;
        public UInt32 Name;
        public UInt32 Base;
        public UInt32 NumberOfFunctions;
        public UInt32 NumberOfNames;
        public UInt32 AddressOfFunctions;       // RVA from base of image
        public UInt32 AddressOfNames;           // RVA from base of image
        public UInt32 AddressOfNameOrdinals;    // RVA from base of image


        // AddressOfFunctions: a RVA to the list of exported functions – it points to an array of NumberOfFunctions DWORD values.
        // Each value is either an RVA pointing to the desired function, or in the case of forwarded functions, an RVA pointing to a forwarding string.

        // AddressOfNames: a RVA to the list of exported names – it points to an array of NumberOfNames DWORD values,
        // each being a RVA to the exported symbol name.

        // AddressOfNameOrdinals: This field is an RVA and points to an array of WORDs.
        // The WORDs are the export ordinals of all the exported functions in this module.
        // However, don’t forget to add in the starting ordinal number specified in the Base field.

        /// <summary>
        /// Finds an exported function from a loaded windows module within its Export Address Table.
        /// Make sure to only use this when NumberOfNames > 0
        /// </summary>
        /// <param name="symbol">The symbol aka function name to search for</param>
        /// <param name="imageBase">The loaded module imagebase (start of dos header/file in memory)</param>
        /// <param name="address">The found function address (RVA) ready-to-use incl imageBase.</param>
        /// <param name="stringComparison">The string comparison to use (optional)</param>
        /// <returns>Successfully found</returns>
        public bool TryGetFunctionAddressBySymbol(
            string symbol,
            IntPtr imageBase,
            out IntPtr address,
            StringComparison stringComparison = StringComparison.InvariantCultureIgnoreCase)
        {
            address = IntPtr.Zero;

            if (string.IsNullOrEmpty(symbol) || imageBase == IntPtr.Zero)
                return false;

            UInt32* nativeFunctionNames = (UInt32*)((byte*)imageBase + AddressOfNames);
            UInt32* nativeFunctionAddresses = (UInt32*)((byte*)imageBase + AddressOfFunctions);
            UInt16* nativeFunctionOrdinals = (UInt16*)((byte*)imageBase + AddressOfNameOrdinals);

            for (int i = 0; i < NumberOfNames; i++)
            {
                byte* currentFunctionName = (byte*)imageBase + nativeFunctionNames[i];

                string? currentFunctionNameManaged = Marshal.PtrToStringAnsi((IntPtr)currentFunctionName);
                if (currentFunctionNameManaged == null)
                    continue;

                if (currentFunctionNameManaged.Equals(symbol, stringComparison))
                {
                    // Map ordinal to function address (we will use this as an index for the function address table)
                    UInt16 functionIndex = nativeFunctionOrdinals[i];
                    //Globals.mls.LogDebug($"functionIndex: {functionIndex}");

                    IntPtr currentFunctionAddress = (IntPtr)((byte*)imageBase + nativeFunctionAddresses[functionIndex]);
                    //Globals.mls.LogDebug($"Quried exported function found: {currentFunctionNameManaged} = {currentFunctionAddress.ToString("X")}");
                    address = currentFunctionAddress;
                    return true;
                }
            }

            return false;
        }
    }

    public enum IMAGE_DIRECTORY_ENTRY_CONSTANT : byte
    {
        EXPORT = 0,             // Export Directory
        IMPORT = 1,             // Import Directory
        RESOURCE = 2,           // Resource Directory
        EXCEPTION = 3,          // Exception Directory
        SECURITY = 4,           // Security Directory
        BASERELOC = 5,          // Base Relocation Table
        DEBUG = 6,              // Debug Directory
        COPYRIGHT = 7,          // (X86 usage)
        ARCHITECTURE = 7,       // Architecture Specific Data
        GLOBALPTR = 8,          // RVA of GP
        TLS = 9,                // TLS Directory
        LOAD_CONFIG = 10,       // Load Configuration Directory
        BOUND_IMPORT = 11,      // Bound Import Directory in headers
        IAT = 12,               // Import Address Table
        DELAY_IMPORT = 13,      // Delay Load Import Descriptors
        COM_DESCRIPTOR = 14     // COM Runtime descriptor
    }
}

