﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{
    public unsafe struct DoublyLinkedListWin32<T> where T : unmanaged
    {
        public delegate void traverse_delegate(T* data);

        /// <summary>
        /// Begin traversal of a doublylinkedlist starting from a defined node
        /// </summary>
        /// <param name="node"></param>
        /// <param name="customFunction"></param>
        /// <param name="shouldContinue"></param>
        /// <param name="dataOffset"></param>
        public static void TraverseFromNode(DoublyLinkedListNodeWin32* node, traverse_delegate customFunction, CancellationToken shouldContinue, nint dataOffset)
        {
            //Console.WriteLine($"TraverseFromNode node: {new IntPtr(node).ToString("X8")}");
            DoublyLinkedListNodeWin32* current = node;
            while (current != null && !shouldContinue.IsCancellationRequested)
            {
                //Console.WriteLine($"TraverseFromNode node query: {new IntPtr(current->Flink).ToString("X8")}");
                //Console.WriteLine($"TraverseFromNode node query + offset: {new IntPtr((uint)current->Flink + (uint)dataOffset).ToString("X8")}");
                //Console.WriteLine($"TraverseFromNode node offset: {dataOffset.ToString("X8")}");
                T* data = (T*)(IntPtr)((ulong)current->Flink + (ulong)dataOffset);
                customFunction(data);

                current = current->Flink;
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct DoublyLinkedListNodeWin32
    {
        public DoublyLinkedListNodeWin32* Flink;
        public DoublyLinkedListNodeWin32* Blink;
    }
}
