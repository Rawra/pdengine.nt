﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct UNICODE_STRING
    {
        public UInt16 Length;
        public UInt16 MaximumLength;
        public IntPtr Buffer;

        public override string ToString()
        {
            // Convert the buffer (a WCHAR* pointer) to a managed string
            // Length is in bytes, divide by 2 for number of WCHARs
            return Marshal.PtrToStringUni(Buffer, Length / 2);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct LIST_ENTRY
    {
        public IntPtr Flink;
        public IntPtr Blink;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RTL_BALANCED_NODE
    {
        LIST_ENTRY Children;
        public IntPtr Left;
        public IntPtr Right;
        public byte Red;
        public byte Balance;
        public IntPtr ParentValue;
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct LARGE_INTEGER
    {
        [FieldOffset(0)]
        public LowHighStruct LowHigh;

        [FieldOffset(0)]
        public long QuadPart;

        [StructLayout(LayoutKind.Sequential)]
        public struct LowHighStruct
        {
            public uint LowPart;
            public int HighPart;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct ULARGE_INTEGER
    {
        [FieldOffset(0)]
        public LowHighStruct LowHigh;

        [FieldOffset(0)]
        public ulong QuadPart;

        [StructLayout(LayoutKind.Sequential)]
        public struct LowHighStruct
        {
            public uint LowPart;
            public uint HighPart;
        }
    }
}
