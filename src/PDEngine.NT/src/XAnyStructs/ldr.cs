﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDEngine.NT.XAnyStructs
{
    public enum LDR_DLL_LOAD_REASON : Int32
    {
        LoadReasonStaticDependency,
        LoadReasonStaticForwarderDependency,
        LoadReasonDynamicForwarderDependency,
        LoadReasonDelayloadDependency,
        LoadReasonDynamicLoad,
        LoadReasonAsImageLoad,
        LoadReasonAsDataLoad,
        LoadReasonEnclavePrimary,
        LoadReasonEnclaveDependency,
        LoadReasonPatchImage,
        LoadReasonUnknown = -1
    }

    public enum LDR_HOT_PATCH_STATE : Int32
    {
        LdrHotPatchBaseImage,
        LdrHotPatchNotApplied,
        LdrHotPatchAppliedReverse,
        LdrHotPatchAppliedForward,
        LdrHotPatchFailedToPatch,
        LdrHotPatchStateMax
    }
}
