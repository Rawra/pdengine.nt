# PDEngine.NT


## What it is

PDEngine.NT is a small but handy tool for working with the PEB block of a process.

The PEB or Process Environment Block contains a lot of useful information that is usually not trivial to retrieve without some WinAPI interaction.

This library uses NtQueryInformationProcess to retrieve the PEB, but there is an alternative to retrieve it via the TEB (Thread Environment Block) instead.

## Features

- Retrieveal of TEB, PEB, LDR easily
- Some useable x86 Windows 11+ compatible kernel structures
- Utility functions for traversing the InMemoryOrderLoaded doubly linked list for the LDR Data Table Entries (by filename)
- Utility function for adding custom library search paths via path and by hooking low level system functions to search other places aswell (LdrpLoadDll)

## Examples

There is a small showcase app included in the repo "NTExApp" but here is a small overview nonetheless:

```cs
	PDProcessExplorer32.Instance.Initialize("PDEngine.NTExApp.exe");

	Console.WriteLine($"TEB Address:\t\t\t0x{PDEngine.NT.Utility.TEB.GetTEB().ToString("X8")}");
	Console.WriteLine($"PEB Address:\t\t\t0x{PDEngine.NT.Utility.PEB.GetPEBByTEB().ToString("X8")}");
	Console.WriteLine($"Image Base Address:\t\t0x{PDProcessExplorer32.Instance.ImageBase.ToString("X8")}");
	Console.WriteLine($"Image Size:\t\t\t0x{PDProcessExplorer32.Instance.SizeOfImage.ToString("X8")}");
	Console.WriteLine($"LDR Address:\t\t\t0x{PDProcessExplorer32.Instance.PEB->Ldr.ToString("X8")}");

	Console.WriteLine("Any key to exit");
	Console.ReadLine();
```

## Building

This project is mostly a standard sln with csproj in SDK Format.

Special rules apply for pdengine.nt.hookdispatcher that is part of the PDEngine.NT.LDR component. That project uses CMAKE to create a native binary that will then be used via PInvoke by the LDR module.

Building of the pdengine.nt.hookdispatcher module is simple:
```cmake .``` in its directory and cmake should do the rest. For this step to succeed you must have cloned this repository recursively for all the submodules/dependencies to be cloned and compiled aswell.